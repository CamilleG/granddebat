package graph;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

public class Tools4Dev {

	/**
	 * Trier le fichier des stopwords
	 * 
	 * @param listToSort
	 * @param fileName
	 */
	public static void ecrireFichierStopwords(Set<String> listToSort, String fileName) {

		// Creating a List of HashSet elements

		List<String> list = new ArrayList<String>(listToSort);

		Collections.sort(list);

		try {

			File f = new File(ToolsText.TOOLPATH + fileName + ".txt");

			f.createNewFile();

			FileWriter fw = new FileWriter(f);

			for (String s : list) {

				fw.write(s + "\n");

			}

			fw.flush();

			fw.close();

		} catch (IOException ex) {

			ex.printStackTrace();

		}

	}

	public static void reecrireMWE(Set<String> listToSort, String fileName) {

		// Creating a List of HashSet elements

		List<String> list = new ArrayList<String>(listToSort);

		Collections.sort(list);

		try {

			File f = new File(ToolsText.TOOLPATH + fileName + ".txt");

			f.createNewFile();

			FileWriter fw = new FileWriter(f);

			for (String s : list) {

				int indexDeb = s.indexOf(";");
				int indexFin = s.lastIndexOf(";");
				if (indexDeb >= 0 && indexFin >= 0)
					fw.write(s.substring(indexDeb + 1, indexFin) + "\n");

			}

			fw.flush();

			fw.close();

		} catch (IOException ex) {

			ex.printStackTrace();

		}

	}

	public static final int THRESHOLD_NBR_OJECTS_JSON = 200;

	/**
	 * Cut big json file in many file smaller
	 * 
	 * @param jsonFile
	 */
	protected static void cuttingFileJson(File jsonFile) {
		int counter = 0;
		int numberFile = 1;

		FileReader fr;
		try {
			fr = new FileReader(jsonFile);
			BufferedReader br = new BufferedReader(fr);
			// while file isn't EOF
			String line = br.readLine();
			line = br.readLine();

			while (line != null) {
				System.out.println("Cut in file : " + numberFile + jsonFile.getName());
				// Create file with name + numberFile
				String nameFile = App.DATAPATH + numberFile + jsonFile.getName();
				PrintWriter writer = new PrintWriter(nameFile, "UTF-8");
				// while file isn't EOF & counter < 10000
				writer.println("[");
				while (line != null && counter < THRESHOLD_NBR_OJECTS_JSON) {
					if (line.startsWith("{")) {
						// It's an object
						// Write in new file the object
						writer.println(line);
						if (counter < THRESHOLD_NBR_OJECTS_JSON - 1) {
							writer.println(",");
						}
						counter++;
					}
					line = br.readLine();
				}
				writer.println("]");
				counter = 0;
				numberFile++;
				writer.close();
			}

			br.close();
			fr.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
