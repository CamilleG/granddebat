package graph;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import org.annolab.tt4j.TreeTaggerWrapper;
import org.graphstream.graph.ElementNotFoundException;
import org.graphstream.graph.Graph;
import org.graphstream.stream.GraphParseException;

import requeterrezo.ConfigurationException;
import requeterrezo.ConfigurationSQL;
import requeterrezo.RequeterRezo;
import requeterrezo.RequeterRezoSQL;

/**
 * Hello world!
 *
 */
public class App {

	public static final String DATAPATH = "../data/";
	public static final String EXPORTPATH = "../outputMulti/";
	public static final String RULESPATH = "../rules/";
	public static final String FILEPATH = "littleFile/3CUT2_1LA_FISCALITE_ET_LES_DEPENSES_PUBLIQUES.json";

	/**
	 * method to ask a data file from an user
	 * 
	 * @return File
	 **/
	protected static File askFileUser() {
		Scanner saisieUtilisateur = new Scanner(System.in);
		String str = null;

		do {
			System.out.println("Veuillez saisir un nom de fichier :");
			str = saisieUtilisateur.next();
		} while (str.trim().isEmpty());
		File fichier = new File(App.DATAPATH + str + ".json");

		saisieUtilisateur.close();
		return fichier;
	}

	/**
	 * write a top of word in a file
	 * 
	 * @param occurrences
	 * @param fileName    of export file
	 */
	protected static void writeListInFile(ArrayList<Pair> occurrences, String fileName) {
		try {
			File f = new File(App.DATAPATH + fileName + ".txt");
			f.createNewFile();
			FileWriter fw = new FileWriter(f);
			for (Pair p : occurrences) {
				fw.write(p.toSimpleString() + "\n");
			}
			fw.flush();
			fw.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	protected static Graph readGraphFile(String filename) {
		Graph g = new GraphCss(filename);
		try {
			g.read(App.EXPORTPATH + filename + ".dgs");
		} catch (ElementNotFoundException | IOException | GraphParseException e) {
			e.printStackTrace();
		}
		return g;
	}

	public static RequeterRezo getRequeterRezo() {
		ConfigurationSQL config;
		try {
			config = new ConfigurationSQL("lib/RequeterRezo/RequeterRezo.ini");
			RequeterRezo rezo = new RequeterRezoSQL(config);
			return rezo;
		} catch (IOException | ConfigurationException e) {
			System.err.println("Error : Impossible to load the configuration !");
			e.printStackTrace();
			return null;
		}
	}

	private static Graph buildGraphData(String[] args) {
		TreeTaggerWrapper<String> tt = new TreeTaggerWrapper<String>();
		System.setProperty("treetagger.home", "../ressources/tree-tagger");
		try {
			tt.setModel("../ressources/lib/french.par:utf-8");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String file;
		if (args.length > 0) {
			file = App.DATAPATH + args[0];
		} else {
			file = App.DATAPATH + App.FILEPATH;
		}

		Theme theme = new Theme(new File(file), new ToolsText(getRequeterRezo()));
		theme.testPosTaggingResponse(tt, "." + File.separator, false);

		GraphCss graph = theme.getGraphWithTraitements(1);

		return graph;
	}

	private static void testRuleParse(String[] args) {
		Graph graph = buildGraphData(args);

		String filePath;
		if (args.length > 1) {
			filePath = App.RULESPATH + args[1];
		} else {
			filePath = App.RULESPATH + "grammaire.txt";
		}
		File fichier = new File(filePath);

		RuleFile rule_file = new RuleFile(fichier, getRequeterRezo());

		rule_file.apply(graph);
		graph.display();
	}

	public static void main(String[] args) {
		System.out.println("Début");

		testRuleParse(args);

		System.out.println("terminé !");
	}
}
