package graph;

import java.util.ArrayList;

public class TripletMakeNode extends Triplet {

	////////////////
	// Attributes //
	////////////////
	private String var;
	private ArrayList<String> concatNode;

	//////////////////
	// Constructors //
	//////////////////
	public TripletMakeNode(String input) {
		this.concatNode = new ArrayList<String>();

		String[] s_r_d = input.split(" ");
		if (s_r_d.length != 3) {
			throw new IllegalArgumentException("Error : " + input + " bad format !");
		} else {
			this.var = s_r_d[0];
			String concatNodeString = s_r_d[2];
			String[] concatN = concatNodeString.split("[.]");
			for (String s : concatN) {
				this.concatNode.add(s);
			}
		}
	}

	////////////////
	// Getters //
	////////////////

	@Override
	public String getType() {
		return "TripletMakeNode";
	}

	public String getNodeName() {
		return var;
	}

	public ArrayList<String> getConcatNode() {
		return concatNode;
	}

	/////////////
	// Methods //
	/////////////
	@Override
	public String toString() {
		return this.getNodeName() + " == " + this.getConcatNode();
	}

}
