package graph;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

import org.graphstream.graph.Graph;

import requeterrezo.RequeterRezo;

public class RuleFile {

	////////////////
	// Attributes //
	////////////////
	private ArrayList<Rule> rules;

	//////////////////
	// Constructors //
	//////////////////
	RuleFile(File file, RequeterRezo rezo) {
		this.rules = new ArrayList<Rule>();

		try {
			Scanner sc = new Scanner(file);

			String line;
			while (sc.hasNextLine()) {
				String rule_string = "";

				while (sc.hasNextLine() && !(line = sc.nextLine()).isEmpty()) {
					if (!line.strip().startsWith("/")) {
						rule_string += line.strip() + " ";
					}
				}

				if (!rule_string.strip().equals("")) {
					rules.add(new Rule(rule_string, rezo));
				}
			}
			sc.close();
		} catch (FileNotFoundException e) {
			System.err.println("Error : Impossible to parse rule file !");
			e.printStackTrace();
		}
	}

	/////////////
	// Methods //
	/////////////
	public void apply(Graph graph) {
		Boolean modif = true;

		while (modif) {
			modif = false;

			for (Rule rule : this.rules) {
				if (rule.apply(graph)) {
					modif = true;
				}
			}
		}
	}

	@Override
	public String toString() {
		String result = "[";

		for (int i = 0; i < this.rules.size(); i++) {
			Rule rule = this.rules.get(i);
			result += rule.toString();

			if (i + 1 != this.rules.size()) {
				result += "\n";
			}
		}

		result += "]";
		return result;
	}
}
