package graph;

/**
 * 
 * 
 * @author RemiCeres CamilleGosset
 *
 */
public class Pair {

	String term;
	double value;

	public Pair(String term, double value) {
		this.term = term;
		this.value = value;
	}

	public String getTerm() {
		return term;
	}

	public double getValue() {
		return value;
	}

	public void incremente() {
		this.value++;
	}

	public void setValue(double value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return term + ";" + value;
	}

	public String toSimpleString() {
		return term.replaceAll("_", " ") + ";" + Math.round(value);
	}

}