package graph;

import java.util.ArrayList;

/**
 * Question associated to set of answers
 * 
 * @author RemiCeres, CamilleGosset
 *
 */
public class Question {

	////////////////////////
	// private attributes //
	////////////////////////
	private String id;
	private String entitled;
	private Boolean isMcq;
	private ArrayList<Response> answers;

	private ToolsText toolsTexte;

	//////////////////
	// constructors //
	//////////////////
	/**
	 * Build question from it's id and entitled
	 * 
	 * @param id        question identification
	 * @param entitled  question entitled
	 * @param isQmc     true if question is a Qmc
	 * @param toolsText tools for treatment of text
	 */
	public Question(String id, String entitled, boolean isQcm, ToolsText toolsTexte) {
		this.answers = new ArrayList<>();
		this.id = id;
		this.entitled = entitled;
		this.isMcq = isQcm;
		this.toolsTexte = toolsTexte;
	}

	/////////////////////
	// getter / setter //
	/////////////////////
	public String getId() {
		return id;
	}

	public String getEntitled() {
		return entitled;
	}

	public ArrayList<Response> getAnswers() {
		return answers;
	}

	/////////////
	// methods //
	/////////////

	/**
	 * add an answer to the question
	 * 
	 * @param answer
	 */
	public void addAnswer(String answer) {
		ArrayList<String> tokeniseAnswer = this.toolsTexte.tokenise(answer);
		Response response = new Response(tokeniseAnswer, this.getId(),
				String.valueOf(this.answers.size())/*
													 * , this.toolsTexte
													 */);
		this.answers.add(response);
	}

	/**
	 * Like toString but return the object in the following json format: Json format
	 * : { idQuestion: "UXVlc3Rpb246MTY3" , intituleQuestion: "Y a-t-il d'autres
	 * points sur les impôts et les dépenses sur lesquels vous souhaiteriez vous
	 * exprimer ?" QCM: "true", reponses: [ "..." , "..." , "..." ] }
	 * 
	 * @return
	 */
	public String toJson() {
		StringBuffer result = new StringBuffer();
		result.append("{");
		result.append(" idQuestion: ");
		result.append(this.getId() + " , ");
		result.append("intituleQuestion: ");
		result.append(this.getEntitled() + " , ");
		result.append("QCM : ");
		result.append(this.isMcq + " , ");
		result.append("reponses: ");
		result.append(this.getAnswers());
		result.append("}");
		return result.toString() + "\n";
	}

	/**
	 * export all graph
	 * 
	 * @param name of input filename
	 */
	public void exportWithTraitements(String filename, ToolsText toolsText) {
		for (Response graph : this.answers) {
			graph.exportWithTraitements(filename, toolsText);
		}
	}

	@Override
	public String toString() {
		String result = "";

		// mcq
		if (this.isMcq) {
			result += "[MCQ] ";

		}
		// properties
		result += this.id + "—" + this.entitled + "\n";

		for (Response answer : answers) {
			result += "\t";
			result += answer.toString();
			result += "\n";
		}

		return result;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Question other = (Question) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}