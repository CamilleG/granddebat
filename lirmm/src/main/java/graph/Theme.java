package graph;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.annolab.tt4j.TreeTaggerWrapper;

import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;

public class Theme {

	////////////////////////
	// private attributes //
	////////////////////////
	private HashMap<String, Question> questions;
	private ToolsText toolsTexte;
	private String filename;

	//////////////////
	// constructors //
	//////////////////

	/**
	 * Parse the data json file in a Questions arrayList
	 *
	 * @param jsonFile grand debat data json file
	 */
	Theme(File jsonFile, ToolsText toolsTexte) {
		this.filename = jsonFile.getName().substring(0, jsonFile.getName().indexOf("."));
		this.toolsTexte = toolsTexte;
		this.questions = new HashMap<String, Question>();

		this.parseQuestion(jsonFile);
	}

	Theme(HashMap<String, Question> q, ToolsText toolsTexte) {
		this.toolsTexte = toolsTexte;
		this.questions = q;
	}

	/**
	 * Parse a json question and add to attributes questions
	 *
	 * @param questionJson
	 */
	private void parseQuestion(File jsonFile) {

		try {
			InputStream stream = new FileInputStream(jsonFile);
			JsonReader reader = new JsonReader(new InputStreamReader(stream, "UTF-8"));

			String questionId = "";
			String questionTitle = "";
			Boolean isMCQ = false;
			String answer = "";

			// read array of forms
			reader.beginArray();

			// each form
			while (reader.hasNext()) {
				questionId = "";
				questionTitle = "";
				isMCQ = false;
				answer = "";
				reader.beginObject();

				// each element in responses
				while (reader.hasNext()) {

					String name = reader.nextName();

					if (name.equals("responses")) {
						// read array
						reader.beginArray();

						// each question response
						while (reader.hasNext()) {
							answer = "";
							reader.beginObject();

							while (reader.hasNext()) {
								name = reader.nextName();
								if (reader.peek() != JsonToken.NULL) {
									if (name.equals("questionId")) {
										questionId = reader.nextString();
									} else if (name.equals("questionTitle")) {
										questionTitle = reader.nextString();
									} else if (name.equals("value")) {
										// determine if is a MCQ and get answer
										String value = reader.nextString();

										// is a MCQ with other response
										int indexMCQ = value.indexOf("\"other\":");
										if (indexMCQ != -1) {
											isMCQ = true;
											value = value.substring(indexMCQ + 8, value.length() - 1);
										}
										// is a open question
										value = this.detectEmptyAnswer(value);
										if (!value.trim().isEmpty()) {
											answer = value;
										}
									} else if (name.equals("formattedValue")) {
										reader.nextString();
									}
								} else {
									reader.nextNull();
								}
							}
							reader.endObject();
							if (!answer.trim().isEmpty()) {
								if (questions.containsKey(questionId)) {
									this.questions.get(questionId).addAnswer(answer);
								} else {
									Question question = new Question(questionId, questionTitle, isMCQ, this.toolsTexte);
									question.addAnswer(answer);
									questions.put(questionId, question);
								}
							}
						}

						reader.endArray();

					} else {
						if (reader.peek() == JsonToken.STRING) {
							reader.nextString();
						} else if (reader.peek() == JsonToken.BOOLEAN) {
							reader.nextBoolean();
						} else if (reader.peek() == JsonToken.NULL) {
							reader.nextNull();
						}
					}

				}
				reader.endObject();
			}

			reader.endArray();

			reader.close();

		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

	/**
	 * get list of questions
	 *
	 * @return list of questions
	 */
	Collection<Question> getQuestions() {
		return this.questions.values();
	}

	/**
	 * Remove empty answer
	 *
	 * @param answer
	 * @return well formatted answer
	 */
	private String detectEmptyAnswer(String answer) {

		if (!answer.equals("null") && !answer.trim().isEmpty() && !answer.equals("\" \"")) {
			answer = answer.replaceAll(System.lineSeparator(), " ");
			answer = answer.replaceAll("\n", " ");

			if (answer.length() > 2 && answer.charAt(0) == '\"' && answer.charAt(answer.length() - 1) == '\"') {
				answer = answer.substring(1, answer.length() - 1);
			}
			return answer;
		}

		return "";
	}

	/**
	 * apply a list of preprocessings in every responses
	 *
	 * @param preTraitements
	 */
	public void applyPreprocessings2(int nbThreads) {
		ScheduledExecutorService executor = Executors.newScheduledThreadPool(nbThreads);
		ArrayList<Response> responses = new ArrayList<>();
		int initialDelay = 0;
		int period = 1;

		for (Question question : questions.values()) {
			responses.addAll(question.getAnswers());
		}

		int elementNber = responses.size() / nbThreads;
		final int N = responses.size();
		for (int i = 0; i < N; i += elementNber) {
			List<Response> newList = responses.subList(i, Math.min(N, i + elementNber));
			TransformToGraph transformToGraph = new TransformToGraph(newList, new ToolsText(this.toolsTexte),
					this.filename);
			executor.scheduleAtFixedRate(transformToGraph, initialDelay, period, TimeUnit.SECONDS);
		}

	}

	/**
	 * export all graph
	 *
	 * @param name of input filename
	 */
	public void exportWithTraitements() {
		for (Question graph : this.questions.values()) {
			graph.exportWithTraitements(filename, toolsTexte);
		}
	}

	/**
	 * Export theme json formated in a new file with filename name
	 *
	 * Json format : { idQuestion: "UXVlc3Rpb246MTY3" , intituleQuestion: "Y a-t-il
	 * d'autres points sur les impôts et les dépenses sur lesquels vous souhaiteriez
	 * vous exprimer ?" QCM: "true", responses: [ "..." , "..." , "..." ] }
	 *
	 * @param filename
	 */
	public void toJson() {

		// create file
		File f = new File(App.DATAPATH + filename + ".json");
		try {
			f.createNewFile();
			FileWriter fw = new FileWriter(f);

			fw.write("[");
			int count = 0;
			for (String questionID : this.questions.keySet()) {
				if (count > 0) {
					fw.write("," + "\n");
				}
				fw.write(this.questions.get(questionID).toJson());
				fw.flush();
				count++;
			}

			fw.write("]");
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void testPosTaggingResponse(TreeTaggerWrapper<String> tt, String outputBase, Boolean export) {
		ArrayList<Response> responses = new ArrayList<>();
		for (Question question : questions.values()) {
			responses.addAll(question.getAnswers());
		}
		Response r;
		for (int i = 0; i < responses.size(); ++i) {
			if ((i + 1) % 1_000 == 0) {
				System.out.println("\t" + (i + 1) + " / " + responses.size() + " responses...");
			}
			r = responses.get(i);
			r.posTaggerResponse(tt);
			if (export) {
				r.sauvegarderReponse(outputBase + r.getID());
			}
		}
//		System.out.println("work done ! ");
	}

	// TODO : pas faite encore bien : juste pour test
	public GraphCss getGraphWithTraitements(int num) {
		Iterator<Question> it = this.getQuestions().iterator();
		Question q = it.next();
		GraphCss graph = null;
		for (int i = 0; i < Math.min(num, q.getAnswers().size()); i++) {
			graph = q.getAnswers().get(i).getGraphWithTraitements(this.toolsTexte);
		}
		return graph;
	}

}
