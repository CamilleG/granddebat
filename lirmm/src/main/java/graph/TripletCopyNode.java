package graph;

public class TripletCopyNode extends Triplet {

	////////////////
	// Attributes //
	////////////////
	private String source;
	private String nodeToCopy;
	private String label;

	//////////////////
	// Constructors //
	//////////////////
	public TripletCopyNode(String input) {

		String[] s_r_d = input.split(" ");
		if (s_r_d.length != 3) {
			throw new IllegalArgumentException("Error : " + input + " bad format !");
		} else {
			this.source = s_r_d[0];
			this.label = s_r_d[2];
		}
		String relation = s_r_d[1];

		// Keep the variable for copying
		this.nodeToCopy = relation.split(":")[1];
	}

	////////////////
	// Getters //
	////////////////
	@Override
	public String getType() {
		return "TripletCopyNode";
	}

	public String getNodeToCopy() {
		return nodeToCopy;
	}

	public String getSource() {
		return source;
	}

	public String getLabel() {
		return label;
	}

}
