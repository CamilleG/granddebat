package graph;

import java.io.Serializable;

public class PosTagging implements Serializable {

	private static final long serialVersionUID = 1L;
	String token;
	String pos;
	String lemma;

	//////// CONSTRUCTOR /////////
	protected PosTagging(String t, String p, String l) {
		this.token = t;
		this.pos = p;
		this.lemma = l;
	}

	///////// GETTERS ///////////
	protected String getToken() {
		return token;
	}

	protected String getPos() {
		return pos;
	}

	protected String getLemma() {
		return lemma;
	}

	@Override
	public String toString() {
		return "[" + token + "\t" + pos + "\t" + lemma + "]\n";
	}

}
