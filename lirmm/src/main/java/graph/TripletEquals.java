package graph;

public class TripletEquals extends Triplet {

	////////////////
	// Attributes //
	////////////////
	private String variable;
	private String value;

	//////////////////
	// Constructors //
	//////////////////

	public TripletEquals(String input) {
		String[] s_r_d = input.split(" ");

		if (s_r_d.length != 3) {
			throw new IllegalArgumentException("Error : " + input + " bad format !");
		} else {
			this.variable = s_r_d[0];
			this.value = s_r_d[2];
		}
	}

	////////////////
	// Getters //
	////////////////
	@Override
	public String getType() {
		return "TripletEquals";
	}

	public String getVariable() {
		return variable;
	}

	public String getvalue() {
		return value;
	}

	/////////////
	// Methods //
	/////////////
	@Override
	public String toString() {
		return this.variable + " == " + this.value;
	}

}
