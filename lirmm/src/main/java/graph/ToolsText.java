package graph;

import static java.util.Arrays.asList;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.NavigableSet;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.annolab.tt4j.TreeTaggerException;
import org.annolab.tt4j.TreeTaggerWrapper;
import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;

import com.atlascopco.hunspell.Hunspell;

import requeterrezo.Filtre;
import requeterrezo.Mot;
import requeterrezo.Relation;
import requeterrezo.RequeterRezo;
import requeterrezo.Resultat;

public class ToolsText {

	////////////////////////
	// private attributes //
	////////////////////////
	protected static final String TOOLPATH = ".." + File.separator + "ressources" + File.separator;
	protected static final String LOGPATH = ".." + File.separator + "log" + File.separator;

	private Hunspell speller = new Hunspell(TOOLPATH + "fr.dic", TOOLPATH + "fr.aff");

	private HashSet<String> stop_word;
	private HashSet<String> ponctuation;
	private TreeSet<String> compoundWords;
	private HashMap<String, String> equivalencies;
	// permet de construire le dernier char... permet de construire le sous-ensemble
	// des candiats
	private final char maxChar = Character.MAX_VALUE;

	// Requeter Rezo
	private RequeterRezo rezo;

	//////////////////
	// constructors //
	//////////////////

	public ToolsText(RequeterRezo rezo) {
		this.compoundWords = new TreeSet<>(new Comparator<String>() {
			@Override
			public int compare(String s1, String s2) {
				s1 = s1.trim();
				s1 = s1.toLowerCase();
				s2 = s2.trim();
				s2 = s2.toLowerCase();
				return s1.compareTo(s2);
			}
		});
		this.stop_word = new HashSet<String>();
		this.ponctuation = new HashSet<String>();
		this.loadFile("mwe.txt", this.compoundWords);
		this.loadFile("stopwords.txt", this.stop_word);
		this.loadFile("ponctuations.txt", this.ponctuation);
		this.equivalencies = new HashMap<String, String>();
		this.rezo = rezo;

		try {
			this.loadEquivalenciesFile("../ressources/equivalencePosTagging.csv", this.equivalencies);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public ToolsText(ToolsText t) {
		this.compoundWords = t.getCompoundWords();
		this.stop_word = t.getStop_word();
		this.ponctuation = t.getPonctuation();
	}

	/**
	 * Load file and put each line in a Set
	 * 
	 * @param fileName
	 * @param toFill
	 */
	private void loadFile(String fileName, Set<String> toFill) {
		// charge list of stop-word
		Scanner scanner;
		try {
			scanner = new Scanner(new File(ToolsText.TOOLPATH + fileName));

			while (scanner.hasNextLine()) {
				String word = scanner.nextLine();
				toFill.add(word);
			}

			// Close the scanner
			scanner.close();
		} catch (FileNotFoundException e) {
			System.err.println("Impossible to find file : " + ToolsText.TOOLPATH + fileName);
			e.printStackTrace();
		}
	}

	/**
	 * Load on a file equivalencies For example : posTagging on Treetagger with
	 * equivalencies on jeuxDeMots
	 * 
	 * @param fileName
	 * @param toFill
	 * @throws IOException
	 */
	private void loadEquivalenciesFile(String fileName, HashMap<String, String> toFill) throws IOException {
		File file = new File(fileName);
		BufferedReader br = null;
		try {
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			// while file isn't EOF
			String line = br.readLine();
			String[] posList;
			while (line != null) {
				posList = line.split(",");
				// System.out.println(posList[0]+ " ---- "+posList[1] + " ---- "+posList[2]);
				toFill.put(posList[0], posList[1]);
				line = br.readLine();
			}
		} finally {
			br.close();
		}
	}

	/////////////
	// getters //
	/////////////

	public HashSet<String> getStop_word() {
		return stop_word;
	}

	public HashSet<String> getPonctuation() {
		return ponctuation;
	}

	public TreeSet<String> getCompoundWords() {
		return compoundWords;
	}

	public HashMap<String, String> getEquivalencies() {
		return equivalencies;
	}

	/////////////
	// methods //
	/////////////

	/**
	 * transforms a sentence into a arrays of words
	 *
	 * @param documentText
	 * @return a arrays of words
	 */
	public ArrayList<String> tokenise(String documentText) {
		ArrayList<String> tokens = new ArrayList<String>();

		// Pattern stuff = Pattern.compile("" + "[\\p{L}]+'*|" + "[\\s]|" + "[0-9]+|" +
		// "([0-9]+\\s)+" + "[(\\p{L}\\.)+\\p{L}]|" + ".");

		Pattern stuff = Pattern.compile("" + "(\\w+('|’))|" + "[\\p{L}]+'*|" + "[\\s]|" + "([0-9]+\\s)+|" + "[0-9]+|"
				+ "[(\\p{L}\\.)+\\p{L}]|" + ".");

		Matcher matcher = stuff.matcher(documentText);
		String find;
		while (matcher.find()) {
			find = matcher.group(0);
			if (!find.matches("\\s")) {
				tokens.add(matcher.group(0).trim()); // add match to the list
			}
		}

		return tokens;
	}

	/**
	 * Transform a table of word in a sentence.
	 *
	 * @param answer
	 * @return string of word separated by space
	 */
	public String deTokenise(ArrayList<String> answer) {
		return String.join(" ", answer);
	}

	public static String staticDeTokenise(ArrayList<String> answer) {
		return String.join(" ", answer);
	}

	/**
	 * If a sequence of words is compound words, create a path in graph of this
	 * compound words Gather the compound words in the sentence together
	 *
	 * @param sentence
	 * @return
	 */
	public void gatherCompoundWords(Graph sentence) {
		String graphName = sentence.getId();
		String graphNber = graphName.split("_")[1];

		// Le sous-ensemble contenant tous les candidats
		NavigableSet<String> subSet;

		// ArrayList<String> tokens = sentence;
		String token;
		int i;

		i = 0;
		// pas la peine de chercher un mot composé commençant au dernier token.

		Node currentNode = sentence.getNode("Text_" + i + "_" + graphNber);

		while (currentNode != null) {
			ArrayList<String> parameters = new ArrayList<>();
			parameters.add("value");
			parameters.add("lemma");
			parameters.add("correction");

			token = "";
			subSet = null;
			Set<SimpleEntry<String, Integer>> solutions = new LinkedHashSet<SimpleEntry<String, Integer>>();
			this.backTrackCompoundWords(subSet, token, sentence, currentNode, parameters, solutions, 0);

			// add new edges
			int finalPosition;
			int k = 0;
			for (SimpleEntry<String, Integer> sol : solutions) {
				finalPosition = sol.getKey().split(" ").length + i + sol.getValue();
				Node previousNode = sentence.getNode("Text_" + (i - 1) + "_" + graphNber);
				Node ultimateNode = sentence.getNode("Text_" + finalPosition + "_" + graphNber);
				Node newNode = sentence.addNode("MC_" + i + "_" + k + "_" + graphNber);
				newNode.addAttribute("ui.label", sol.getKey());
				newNode.addAttribute("value", sol.getKey());
				newNode.addAttribute("ui.class", "compoundWords");
				if (previousNode == null) {
					previousNode = sentence.getNode("start_" + graphNber);
				}
				Edge edge = sentence.addEdge("r_mc_" + previousNode + "_" + k + "_" + graphNber, previousNode, newNode,
						true);
				edge.addAttribute("ui.class", "r_mc");
				edge.addAttribute("value", "r_mc");

				if (ultimateNode == null) {
					ultimateNode = sentence.getNode("end_" + graphNber);
				}
				Edge edge2 = sentence.addEdge("r_mc_" + newNode + "_" + graphNber, newNode, ultimateNode, true);
				edge2.addAttribute("ui.class", "r_mc");
				edge2.addAttribute("value", "r_mc");
				k++;
			}

			i++;
			currentNode = sentence.getNode("Text_" + i + "_" + graphNber);
			// bestCandidate contient ici :
			// soit le terme original
			// soit le plus long ngram en partant de la gauche présent dans rezoJDM
		}
	}

	/**
	 * test all combinations of the word forms
	 *
	 * @param subSet      compound word possible
	 * @param token       compound word start
	 * @param graph
	 * @param currentNode end node of compound word
	 * @param parameters  name of forms
	 * @param solutions   all compound words
	 */
	private void backTrackCompoundWords(NavigableSet<String> subSet, String token, Graph graph, Node currentNode,
			ArrayList<String> parameters, Set<SimpleEntry<String, Integer>> solutions, Integer adverbCounter) {
		// if token is find
		if (subSet != null && subSet.contains(token)) {
			solutions.add(new SimpleEntry<String, Integer>(subSet.pollFirst(), adverbCounter));
		}

		// add all forms of words
		if (currentNode != null) {
			Set<String> forms = new HashSet<String>();
			for (String parameter : parameters) {
				// la|le
				Object ob = currentNode.getAttribute(parameter);
				if (ob instanceof String) {
					String tempForm = currentNode.getAttribute(parameter);
					if (tempForm != null) {
						String[] allLemm = tempForm.split("\\|");
						for (String s : allLemm) {
							forms.add(s);
						}
					} else {
						System.out.println(currentNode + " is null");
						System.out.println(currentNode.getAttribute("value") + " have null value");
					}
				}else {
					ArrayList<String> allFormAttribute = new ArrayList<>();
					allFormAttribute.addAll(currentNode.getAttribute(parameter));
					for(String tempForm : allFormAttribute) {
						if (tempForm != null) {
							String[] allLemm = tempForm.split("\\|");
							for (String s : allLemm) {
								forms.add(s);
							}
						} else {
							System.out.println(currentNode + " is null");
							System.out.println(currentNode.getAttribute("value") + " have null value");
						}
					}
				}			
				
			}

			Iterator<Node> neighbors = currentNode.getNeighborNodeIterator();
			Node neighbor;
			String posTT = null;
			while (neighbors.hasNext()) {
				neighbor = neighbors.next();
				if (neighbor.getAttribute("ui.class") != null && neighbor.getAttribute("ui.class").equals("tagTT")) {
					posTT = neighbor.getAttribute("value");
				}

			}
			if (posTT != null && posTT.equals("ADV")) {
				forms.add("");
				adverbCounter++;
			}

			for (String word : forms) {
				String newToken = null;
				newToken = token + " " + word;
				newToken = newToken.trim();
				// ligne critique : on récupère le sous-ensemble des mots composés commençant
				// par le terme que l'on construit.
				subSet = compoundWords.subSet(newToken, true, newToken + " " + this.maxChar, false);

				// get next node
				String[] id_current_node = currentNode.getId().split("_");
				String idNextNode = "Text_" + (Integer.parseInt(id_current_node[1]) + 1) + "_" + id_current_node[2];

				if (!subSet.isEmpty()) {
					Node nextNode = graph.getNode(idNextNode);
					this.backTrackCompoundWords(subSet, newToken, graph, nextNode, parameters, solutions,
							adverbCounter);
				}
			}
		}
	}

	/**
	 * add property stop word in a graph
	 *
	 * @param sentence tokens sentence
	 * @return tokens sentence without stop word
	 */
	public void removeStopWord(Graph sentence) {

		String graphName = sentence.getId();
		int i = 0;
		String graphNber = graphName.split("_")[1];
		Node currentNode = sentence.getNode("Text_" + i + "_" + graphNber);

		while (currentNode != null) {
			String word = currentNode.getAttribute("value");
			// word = word.toLowerCase();

			// add only if is not a stop word
			if (this.stop_word.contains(word)) {
				currentNode.addAttribute("isStopWord", true);
				currentNode.addAttribute("ui.class", "isStopWord");
			} else {
				currentNode.addAttribute("isStopWord", false);
			}
			i++;
			currentNode = sentence.getNode("Text_" + i + "_" + graphNber);
		}

	}

	/**
	 * add parameter lemmatisation with JDM
	 *
	 * @param graph
	 */
	public void lemmatisation(Graph graph) {

		// get initial node
		String graphName = graph.getId();
		int i = 0;
		String graphNber = graphName.split("_")[1];
		Node currentNode = graph.getNode("Text_" + i + "_" + graphNber);

		while (currentNode != null) {
			String label = currentNode.getAttribute("value");

			// search in JDM
			Resultat resultatRequete = rezo.requete(label, "r_lemma", Filtre.RejeterRelationsEntrantes);
			Mot mot = resultatRequete.getMot();

			if (mot != null) {
				ArrayList<Relation> relations = mot.getRelationsSortantesTypees("r_lemma");

				ArrayList<String> lemma = new ArrayList<>();
				for (Relation r : relations) {
					String pos = r.getMotFormateDestination();
					lemma.add(pos);
				}
				currentNode.addAttribute("lemmaJDM", lemma);
			}

			// next node
			i++;
			currentNode = graph.getNode("Text_" + i + "_" + graphNber);
		}

	}

	/**
	 * Add on a graph, the tag for each word on the sentence if you need to use this
	 * function, you need to install treeTagger on the following link:
	 * https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/ Follow sequence to
	 * install it
	 *
	 * @param graph
	 * @param graphName
	 */
	public void posTaggerTreeTager(Graph graph) {

		String graphName = graph.getId();
		int i = 0;
		String graphNber = graphName.split("_")[1];
		Node currentNode = graph.getNode("Text_" + i + "_" + graphNber);

		System.setProperty("treetagger.home", "../ressources/tree-tagger");
		TreeTaggerWrapper<String> tt = new TreeTaggerWrapper<String>();
		try {
			tt.setModel("../ressources/lib/french.par:utf-8");

			// Ici on récupère la forme lemmatisée du mot : intéressant pour la
			// lemmatisation

			while (currentNode != null) {
				final int k = i;
				final Node currentNode2 = currentNode;

				tt.setHandler((token, pos, lemma) -> {
					// Ici on a une fnction lambda qu'on va venir adapter à notre graphe:
					// Au lieu d'afficher le token le pos et le lemme, plutot venir le stokcer dans
					// le graphe!
					// System.out.println(token + "\t" + pos + "\t" + lemma));

					// add nodes and edges
					Node tagNode = graph.getNode(pos);
					if (tagNode == null) {
						tagNode = graph.addNode(pos);
						tagNode.addAttribute("ui.label", pos);
						tagNode.addAttribute("value", pos);
						tagNode.addAttribute("ui.class", "tagTT");
						tagNode.addAttribute("source", "treeTagger");
					}

					Edge tagEdge = graph.addEdge("r_pos_" + currentNode2.getId() + "_" + tagNode.getId(), currentNode2,
							tagNode, true);
					tagEdge.addAttribute("ui.class", "r_tag");
					tagEdge.addAttribute("value", "r_tag");

					graph.getNode("Text_" + k + "_" + graphNber).addAttribute("lemma", lemma);
				});

				tt.process(asList(new String[] { currentNode.getAttribute("value") }));
				i++;
				currentNode = graph.getNode("Text_" + i + "_" + graphNber);
			}

		} catch (IOException | TreeTaggerException e) {
			e.printStackTrace();
		} finally {
			tt.destroy();
		}
	}

	/**
	 * PosTagg each word on the graph with JeuxDeMots Create node and links of
	 * posTagging of the word Each possible postagging of the words are create on
	 * the graph
	 * 
	 * @param graph
	 */
	public void posTaggerJDM(Graph graph) {

		// get initial node
		String graphName = graph.getId();
		int i = 0;
		String graphNber = graphName.split("_")[1];
		Node currentNode = graph.getNode("Text_" + i + "_" + graphNber);

		while (currentNode != null) {
			String label = currentNode.getAttribute("value");

			// search in JDM
			Resultat resultatRequete = rezo.requete(label, "r_pos", Filtre.RejeterRelationsEntrantes);
			Mot mot = resultatRequete.getMot();

			if (mot != null) {
				ArrayList<Relation> relations = mot.getRelationsSortantesTypees("r_pos");

				for (Relation r : relations) {

					if (r.getPoids() < 0 || r.getPoids() >= 25) {
						String pos = r.getMotFormateDestination();
						String posSimple = pos.split(":")[0];

						boolean simple = false;
						if (pos.equals(posSimple + ":")) {
							simple = true;
						}

						// add nodes and edges
						Node tagNode = graph.getNode(pos);
						if (tagNode == null) {
							tagNode = graph.addNode(pos);
							tagNode.addAttribute("ui.label", pos);
							tagNode.addAttribute("value", pos);
							if (simple) {
								tagNode.addAttribute("ui.class", "tagJDMSimple");
							} else {
								tagNode.addAttribute("ui.class", "tagJDM");
							}
							tagNode.addAttribute("source", "jdm");
						}

						// prevent the creation of two link in the simple case
						if (graph.getEdge("r_pos_" + currentNode.getId() + "_" + tagNode.getId()) == null) {
							Edge tagEdge = graph.addEdge("r_pos_" + currentNode.getId() + "_" + tagNode.getId(),
									currentNode, tagNode, true);
							tagEdge.addAttribute("poids", r.getPoids());
							tagEdge.addAttribute("value", "r_pos");
							// verify if weight is negative
							if (r.getPoids() < 0) {
								tagEdge.addAttribute("ui.class", "tagNegatif");

								// tagNode.getId());
							} else {
								tagEdge.addAttribute("ui.class", "r_tag");
							}
						}
					}
				}
			}

			// next node
			i++;
			currentNode = graph.getNode("Text_" + i + "_" + graphNber);
		}
	}

	/**
	 * postagging of compound words with jeuxDeMots
	 *
	 * @param graph
	 */
	public void posTaggerJdmCompoundWord(Graph graph) {

		// get initial node
		String graphName = graph.getId();
		int i = 0;
		String graphNber = graphName.split("_")[1];
		Node currentNode = graph.getNode("Text_" + i + "_" + graphNber);

		// "MC_" + i + "_" + k + "_" + graphNber
		while (currentNode != null) {
			int k = 0;
			Node mcCurrentNode = graph.getNode("MC_" + i + "_" + k + "_" + graphNber);
			while (mcCurrentNode != null) {
				String label = mcCurrentNode.getAttribute("value");
				// search in JDM
				Resultat resultatRequete = rezo.requete(label, "r_pos", Filtre.RejeterRelationsEntrantes);
				Mot mot = resultatRequete.getMot();

				if (mot != null) {
					ArrayList<Relation> relations = mot.getRelationsSortantesTypees("r_pos");

					for (Relation r : relations) {

						if (r.getPoids() < 0 || r.getPoids() >= 25) {
							String pos = r.getMotFormateDestination();
							String posSimple = pos.split(":")[0];

							boolean simple = false;
							if (pos.equals(posSimple + ":")) {
								simple = true;
							}

							// add nodes and edges
							Node tagNode = graph.getNode(pos);
							if (tagNode == null) {
								tagNode = graph.addNode(pos);
								tagNode.addAttribute("ui.label", pos);
								tagNode.addAttribute("value", pos);
								if (simple) {
									tagNode.addAttribute("ui.class", "tagJDMSimple");
								} else {
									tagNode.addAttribute("ui.class", "tagJDM");
								}
								tagNode.addAttribute("source", "jdm");
							}

							// prevent the creation of two links in the simple case
							if (graph.getEdge("r_pos_" + mcCurrentNode.getId() + "_" + tagNode.getId()) == null) {
								Edge tagEdge = graph.addEdge("r_pos_" + mcCurrentNode.getId() + "_" + tagNode.getId(),
										mcCurrentNode, tagNode, true);
								tagEdge.addAttribute("poids", r.getPoids());

								tagEdge.addAttribute("ui.class", "r_tag");
								tagEdge.addAttribute("value", "r_tag");
							}
						}
					}
				}
				// next compound word node
				k++;
				mcCurrentNode = graph.getNode("MC_" + i + "_" + k + "_" + graphNber);
			}

			// next node
			i++;
			currentNode = graph.getNode("Text_" + i + "_" + graphNber);
		}
	}

	/**
	 * Verify if postagging which coming from JeuxdeMots corresponds to the
	 * postagging coming from TreeTagger If there is a match, this JeuxDeMots
	 * postagging will be the privileged
	 * 
	 * @param graph
	 * @throws IOException
	 */
	public void verificationPosTagging(GraphCss graph) throws IOException {

		// get graph name
		String graphName = graph.getId();
		String graphNber = graphName.split("_")[1];

		int i = 0;
		Node currentNode = graph.getNode("Text_" + i + "_" + graphNber);

		// Load log file
		File f = new File(LOGPATH + graphName + ".txt");
		if (!f.exists())
			return;
		FileWriter fw = new FileWriter(f, true);

		// Node followingNode;
		while (currentNode != null) {
			Iterator<Node> neighbors = currentNode.getNeighborNodeIterator();
			Node neighbor;
			String posTT = null;
			ArrayList<String> posJDM = new ArrayList<String>();
			ArrayList<Node> neighborsPosJdm = new ArrayList<Node>();

			while (neighbors.hasNext()) {
				neighbor = neighbors.next();
				if (neighbor.getAttribute("ui.class") != null && (neighbor.getAttribute("ui.class").equals("tagJDM")
						|| neighbor.getAttribute("ui.class").equals("tagJDMSimple"))) {
					posJDM.add(neighbor.getAttribute("value"));
					neighborsPosJdm.add(neighbor);
				}
				if (neighbor.getAttribute("ui.class") != null && neighbor.getAttribute("ui.class").equals("tagTT")) {
					posTT = neighbor.getAttribute("value");
				}
			}

			if (posTT != null && this.equivalencies.containsKey(posTT)) {
				String correspondancePosTT = this.equivalencies.get(posTT);
				boolean correspondance = false;
				for (int k = 0; k < posJDM.size(); k++) {
					String s = posJDM.get(k);
					Node n = neighborsPosJdm.get(k);
					Edge e = currentNode.getEdgeBetween(n);
					if (s.startsWith(correspondancePosTT)) {
						correspondance = true;
						// TreeTagger en a 1 et JDM plsuieurs
						// Si match alors sens préféré
						e.setAttribute("ui.class", n.getAttribute("ui.class") + ", " + "tagPositif");
					} else {
						e.setAttribute("ui.class", n.getAttribute("ui.class") + ", " + "tagNegatif");
					}
				}
				if (!correspondance) {
					// JDM différent de TreeTagger
					// Logger dans un fichier : contexte + les pos
					fw.write("Pour \" " + currentNode.getAttribute("value") + "\" le POS ne correspond pas : TT = "
							+ posTT + " correspondancePosTT= " + correspondancePosTT + " et posJDM = " + posJDM + "\n");
				}
			}

			// get next node
			i++;
			currentNode = graph.getNode("Text_" + i + "_" + graphNber);
		}
		fw.flush();

		fw.close();

	}

	/**
	 * Correct words with hunspell library Graph grap
	 *
	 * @param tokenise
	 * @return Strings ArrayList : each word with a correct spelling
	 */
	public void spellCheckingHunspell(Graph graph) {

		String graphName = graph.getId();
		String graphNber = graphName.split("_")[1];
		int i = 0;
		Node currentNode = graph.getNode("Text_" + i + "_" + graphNber);

		// Node followingNode;
		while (currentNode != null) {

			String word = currentNode.getAttribute("value");

			if (!speller.spell(word) && !this.ponctuation.contains(word)) {
				List<String> suggests = speller.suggest(word);
				suggests.remove("franque");
				if (!suggests.isEmpty()) {
					// if a suggest word exist
					word = suggests.get(0);
				}
				// System.out.println(suggests);
			}

			currentNode.addAttribute("correction", word);

			// get next node
			i++;
			currentNode = graph.getNode("Text_" + i + "_" + graphNber);
		}
	}

	/**
	 * Apply upperCaseOrLowerCase foreach node in the graph named "graph"
	 *
	 * @param graph
	 */
	protected void applyRealCaseInGraph(Graph graph) {
		for (Node n : graph) {
			if (!n.getId().startsWith("start_") && !n.getId().startsWith("end_")) {

				this.upperCaseOrLowerCase(n);
			}
		}
	}

	/**
	 * If the lemmatisation's form of the current node is in upperCase the word will
	 * be in upperCase in the lower case otherwise
	 *
	 * @param currentNode
	 */
	protected void upperCaseOrLowerCase(Node currentNode) {
		String specialChars = "~`!@#$%^&*()-_=+\\|[{]};:'\",<.>/?Ãª";
		String lemmaForm = currentNode.getAttribute("lemma");
		String normalForm = currentNode.getAttribute("value");
		if (lemmaForm != null && !lemmaForm.isEmpty() && normalForm != null && !normalForm.isEmpty()) {
			char firstCharacterLemma = lemmaForm.charAt(0);
			// System.out.println("char lemma "+firstCharacterLemma);
			normalForm = normalForm.toLowerCase();
			char firstCharacterNormal = normalForm.charAt(0);
			if (normalForm.length() > 1)
				normalForm = normalForm.substring(1);
			else
				normalForm = "";
			if (!specialChars.contains(String.valueOf(firstCharacterLemma))
					&& Character.isUpperCase(firstCharacterLemma)) {
				firstCharacterNormal = Character.toUpperCase(firstCharacterNormal);
			}
			normalForm = firstCharacterNormal + normalForm;
			currentNode.setAttribute("ui.label", normalForm);
			currentNode.setAttribute("value", normalForm);
		}
	}

	/**
	 * Find the governor in each compound words in a graph For example the compound
	 * word "petit chat" has the governor "chat" : the name of the compound word For
	 * "boire du lait", "boire" is the governor because it's the main verb
	 * 
	 * @param graph
	 */
	public void findGovernorCompoundWords(Graph graph) {

		// get initial node
		String graphName = graph.getId();
		int i = 0;
		String graphNber = graphName.split("_")[1];
		Node currentNode = graph.getNode("Text_" + i + "_" + graphNber);

		// "MC_" + i + "_" + k + "_" + graphNber
		while (currentNode != null) {
			int k = 0;
			Node mcCurrentNode = graph.getNode("MC_" + i + "_" + k + "_" + graphNber);
			while (mcCurrentNode != null) {
				Node governor = null;
				String label = mcCurrentNode.getAttribute("value");
				label = label.trim();
				String[] splitLabel = label.split(" ");
				for (int j = 0; governor == null && j < splitLabel.length; j++) {
					int countWord = (i + j);
					Node n0 = graph.getNode("Text_" + countWord + "_" + graphNber);
					if (n0 != null) {
						Collection<Edge> edges = n0.getLeavingEdgeSet();
						for (Edge edge : edges) {
							if (edge.getAttribute("ui.class").equals("r_tag")) {
								Node n1 = edge.getNode1();
								String s = n1.getAttribute("value");
								if (s.startsWith("NOM")) {
									governor = n0;
								}
								if (s.startsWith("VER")) {
									governor = n0;
								}
							}
						}
					}
				}
				// prevent the creation of two links in the simple case
				if (governor != null
						&& graph.getEdge("r_gov_" + mcCurrentNode.getId() + "_" + governor.getId()) == null) {
					Edge govEdge = graph.addEdge("r_gov_" + mcCurrentNode.getId() + "_" + governor.getId(),
							mcCurrentNode, governor, true);
					govEdge.addAttribute("ui.class", "r_gov");
					govEdge.addAttribute("value", "r_gov");
				}

				// next compound word node
				k++;
				mcCurrentNode = graph.getNode("MC_" + i + "_" + k + "_" + graphNber);
			}

			// next node
			i++;
			currentNode = graph.getNode("Text_" + i + "_" + graphNber);
		}
	}

	/**
	 * On each word on a graph, will make inferences with JeuxDeMots word>refinement
	 * We take the privileged postagging of the word We take every postagging for
	 * the refinement If some postagging of the refinement matches then we keep this
	 * refinement At the end, we take the postagging with the greatest weight
	 * 
	 * @param graph
	 */
	public void inferenceJdm(Graph graph) {
		// get initial node
		String graphName = graph.getId();
		int i = 0;
		String graphNber = graphName.split("_")[1];
		Node currentNode = graph.getNode("Text_" + i + "_" + graphNber);

		while (currentNode != null) {
			String label = currentNode.getAttribute("value");

			// Take pos of the currentNode
			Collection<Edge> edges = currentNode.getLeavingEdgeSet();
			String posCurrentNode = null;
			int poidsMax = Integer.MIN_VALUE;
			for (Edge edge : edges) {
				if (edge.getAttribute("ui.class").equals("r_tag")) {
					Node n1 = edge.getNode1();
					String onePos = n1.getAttribute("value");
					if (n1.getAttribute("source").equals("treeTagger")) {
						posCurrentNode = this.equivalencies.get(onePos);
						break;
					}
					int currentweight = Integer.valueOf(edge.getAttribute("poids"));
					if (currentweight > poidsMax) {
						poidsMax = currentweight;
						posCurrentNode = onePos;
					}
				}
			}

			makeRefinement("r_raff_sem", label, posCurrentNode, graph, currentNode);
			makeRefinement("r_raff_morpho", label, posCurrentNode, graph, currentNode);

			// next node
			i++;
			currentNode = graph.getNode("Text_" + i + "_" + graphNber);
		}
	}

	/**
	 * On each compound word on a graph, will make inferences with JeuxDeMots
	 * word>refinement We take the privileged postagging of the word We take every
	 * postagging for the refinement If some postagging of the refinement matches
	 * then we keep this refinement At the end, we take the postagging with the
	 * greatest weight
	 * 
	 * @param graph
	 */
	public void inferenceJdmCompoundWords(Graph graph) {
		// get initial node
		String graphName = graph.getId();
		int i = 0;
		String graphNber = graphName.split("_")[1];
		Node currentNode = graph.getNode("Text_" + i + "_" + graphNber);

		while (currentNode != null) {
			int k = 0;
			Node mcCurrentNode = graph.getNode("MC_" + i + "_" + k + "_" + graphNber);
			while (mcCurrentNode != null) {
				String label = mcCurrentNode.getAttribute("value");

				// Take pos of the currentNode
				Collection<Edge> edges = mcCurrentNode.getLeavingEdgeSet();
				String posCurrentNode = null;
				int poidsMax = Integer.MIN_VALUE;
				for (Edge edge : edges) {
					if (edge.getAttribute("ui.class").equals("r_tag")) {
						Node n1 = edge.getNode1();
						String onePos = n1.getAttribute("value");

						Integer currentweight = edge.getAttribute("poids");
						if (currentweight > Integer.valueOf(poidsMax)) {
							poidsMax = currentweight;
							posCurrentNode = onePos;
						}
					}
				}
				makeRefinement("r_raff_sem", label, posCurrentNode, graph, mcCurrentNode);
				makeRefinement("r_raff_morpho", label, posCurrentNode, graph, mcCurrentNode);

				// next compound word node
				k++;
				mcCurrentNode = graph.getNode("MC_" + i + "_" + k + "_" + graphNber);
			}

			// next node
			i++;
			currentNode = graph.getNode("Text_" + i + "_" + graphNber);
		}
	}

	/**
	 * Make refinement of the currentNode
	 * 
	 * @param typeRaff
	 * @param label
	 * @param posCurrentNode
	 * @param graph
	 * @param currentNode
	 */
	public void makeRefinement(String typeRaff, String label, String posCurrentNode, Graph graph, Node currentNode) {
		// search in JDM
		Resultat resultatRequete = rezo.requete(label, typeRaff, Filtre.RejeterRelationsEntrantes);
		Mot mot = resultatRequete.getMot();

		if (mot != null) {
			ArrayList<Relation> relations = mot.getRelationsSortantesTypees(typeRaff);
			int poidsMax = Integer.MIN_VALUE;
			Relation relMax = null;
			Set<Relation> potentialRelations = new HashSet<Relation>();
			for (Relation r : relations) {
				String raff = r.getMotFormateDestination().split(">")[1];

				// search in JDM
				Resultat resultatRequeteRaff = rezo.requete(raff, "r_pos", Filtre.RejeterRelationsEntrantes);
				Mot motRaff = resultatRequeteRaff.getMot();

				if (motRaff != null) {
					ArrayList<Relation> relationsRaff = motRaff.getRelationsSortantesTypees("r_pos");
					for (Relation r2 : relationsRaff) {
						String posRaff = r2.getMotFormateDestination();
						if (posRaff.equals("POS:")) {
							posRaff = raff;
						}
						if (posCurrentNode != null && posCurrentNode.startsWith(posRaff)) {
							potentialRelations.add(r);
						}

					}
				}
			}
			for (Relation potentialR : potentialRelations) {
				// search in JDM
				int sensPref = this.rezo.verifierExistenceRelation(potentialR.getNomDestination(), 36,
						"_INFO-MEANING-PREFERED");
				if (sensPref > 0) {
					relMax = potentialR;
					break;
				}

				int poids = potentialR.getPoids();
				if (poidsMax < poids) {
					poidsMax = poids;
					relMax = potentialR;
				}
			}
			// prevent the creation of two links in the simple case
			if (relMax != null) {
				Node superNode = graph.getNode(relMax.getMotFormateDestination().replaceAll(" ", "_"));
				if (superNode == null) {
					superNode = graph.addNode(relMax.getMotFormateDestination().replaceAll(" ", "_"));
					superNode.addAttribute("ui.label", relMax.getMotFormateDestination());
					superNode.addAttribute("value", relMax.getMotFormateDestination());
					superNode.addAttribute("ui.class", "superRaff" + typeRaff.split("_")[2]);
					superNode.addAttribute("source", "jdm");
				}

				// prevent the creation of two links in the simple case
				if (graph.getEdge(typeRaff + "_" + currentNode.getId() + "_" + superNode.getId()) == null) {
					Edge superEdge = graph.addEdge(typeRaff + "_" + currentNode.getId() + "_" + superNode.getId(),
							currentNode, superNode, true);
					superEdge.addAttribute("poids", relMax.getPoids());

					superEdge.addAttribute("ui.class", typeRaff);
					superEdge.addAttribute("value", typeRaff);
				}
			}

		}
	}

	public void close() {
		this.speller.close();
	}
}
