package graph;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.MultiGraph;
import org.graphstream.ui.graphicGraph.GraphicElement;
import org.graphstream.ui.layout.Layout;
import org.graphstream.ui.layout.Layouts;
import org.graphstream.ui.swingViewer.GraphRenderer;
import org.graphstream.ui.swingViewer.View;
import org.graphstream.ui.swingViewer.Viewer;

public class GraphCss extends MultiGraph {

	private final String URL = "../ressources/style.css";

	public GraphCss(String id) {
		super(id);
		System.setProperty("org.graphstream.ui.renderer", "org.graphstream.ui.j2dviewer.J2DGraphRenderer");
		this.hides = new ArrayList<>();
		this.addAttribute("ui.stylesheet", "url('" + this.URL + "')");
		this.addAttribute("ui.quality");
		this.addAttribute("ui.antialias");
	}

	/**
	 * Display a graph
	 *
	 * @param graph
	 */
	@Override
	public Viewer display() {

		Viewer viewer = new Viewer(this, Viewer.ThreadingModel.GRAPH_IN_ANOTHER_THREAD);
		GraphRenderer renderer = Viewer.newGraphRenderer();
		viewer.addView(Viewer.DEFAULT_VIEW_ID, renderer);

		Layout layout = Layouts.newLayoutAlgorithm();
		viewer.enableAutoLayout(layout);

		final View view = viewer.getDefaultView();

		// add key listener
		view.addKeyListener(new KeyListener() {
			public void keyTyped(KeyEvent ke) {
				keyListener(ke, view);
			}

			public void keyPressed(KeyEvent ke) {
			}

			public void keyReleased(KeyEvent ke) {
			}
		});

		// add mouse listener
		view.addMouseWheelListener(new MouseWheelListener() {
			public void mouseWheelMoved(MouseWheelEvent mwl) {
				mouseListener(mwl, view);
			}
		});

		// add click listener
		view.addMouseListener(new MouseListener() {

			@Override
			public void mouseClicked(MouseEvent me) {
				clickListerner(me, view);
			}

			@Override
			public void mouseEntered(MouseEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseExited(MouseEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mousePressed(MouseEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseReleased(MouseEvent arg0) {
				// TODO Auto-generated method stub

			}

		});

		switchHideNode("tagJDM");
		switchHideNode("tagJDMSimple");
		switchHideNode("compoundWords");
		switchHideNode("tagTT");
		switchHideNode("superRaffsem");
		switchHideNode("superRaffmorpho");

		return viewer;
	}

	/*
	 * configure click listener
	 */
	private void clickListerner(MouseEvent me, View view) {
		GraphicElement element = view.findNodeOrSpriteAt(me.getX(), me.getY());

		if (element != null) {
			System.out.println("===================");
			{
				System.out.println("id: " + element.getId());
				for (String att : element.getAttributeKeySet())
					System.out.println(att + " : " + element.getAttribute(att));
			}
		}
	}

	/*
	 * configure mouse listener
	 */
	private void mouseListener(MouseWheelEvent arg0, View view) {
		double viewPercent = view.getCamera().getViewPercent();
		if (arg0.getWheelRotation() > 0) {
			view.getCamera().setViewPercent(viewPercent * 1.1);
		} else {
			view.getCamera().setViewPercent(viewPercent * 0.9);
		}

	}

	private HashMap<String, Node> savHideNodes = new HashMap<>();
	private HashMap<String, ArrayList<Edge>> savHideEdges = new HashMap<>();

	private void hideNode(String category, Node node) {

		// sav and remove edges
		Collection<Edge> edges = node.getEdgeSet();
		Object[] arrayEdges = edges.toArray().clone();

		ArrayList<Edge> saveEdges = new ArrayList<Edge>();
		for (Object edge : arrayEdges) {
			saveEdges.add(this.removeEdge((Edge) edge));
		}
		this.savHideEdges.put(category, saveEdges);

		// sav and remove node
		Node graph2 = this.removeNode(node);
		this.savHideNodes.put(category, graph2);
	}

	private void restoreNode(String category) {
		// get old node
		Node tempNode = this.savHideNodes.get(category);
		this.savHideNodes.remove(category);

		// build new node
		Node node = this.addNode(tempNode.getId());

		// restore nodes attributes
		for (String att : tempNode.getAttributeKeySet()) {
			Object value = tempNode.getAttribute(att);
			node.addAttribute(att, value);
		}

		// restore edges
		for (Edge oldEdge : this.savHideEdges.get(category)) {
			// selection good node
			Node sourceNode;
			Node targetNode;
			if (oldEdge.getSourceNode().getId() == node.getId()) {
				sourceNode = node;
				targetNode = oldEdge.getTargetNode();
			} else if (oldEdge.getTargetNode().getId() == node.getId()) {
				sourceNode = oldEdge.getSourceNode();
				targetNode = node;
			} else {
				sourceNode = oldEdge.getSourceNode();
				targetNode = oldEdge.getTargetNode();
			}

			Edge edge = this.addEdge(oldEdge.getId(), sourceNode, targetNode, oldEdge.isDirected());

			// restore edge attributes
			for (String att : oldEdge.getAttributeKeySet()) {
				String identifiant = att;
				Object value = oldEdge.getAttribute(att);
				edge.addAttribute(identifiant, value);
			}
		}
	}

	private ArrayList<String> hides;

	private void switchHideNode(String category) {

		Collection<Node> nodes = this.getNodeSet();
		Object[] arrayEdges = nodes.toArray().clone();

		Collection<String> idHideNode = this.savHideNodes.keySet();
		Object[] arrayIdHideNode = idHideNode.toArray().clone();

		// if is hide
		if (this.hides.contains(category)) {
			this.hides.remove(category);

			for (Object temp : arrayIdHideNode.clone()) {
				String id = (String) temp;
				if (id.startsWith(category)) {
					this.restoreNode("" + category + this.savHideNodes.get(id));
				}
			}
		} else { // if isn't hide
			this.hides.add(category);

			for (Object temp : arrayEdges) {
				Node node = (Node) temp;
				if (node.getAttribute("ui.class") != null && node.getAttribute("ui.class").equals(category)) {
					this.hideNode("" + category + node, node);
				}
			}
		}
	}

	/*
	 * configure key board listener
	 * 
	 * SPACE : reset camera c : display/hide compound words t : display/hide tag
	 * treeTager j : display/hide tag jdm r : refinement sem or morpho alt+j
	 * :display/hide simple jdm tag
	 */
	private void keyListener(KeyEvent arg0, View view) {
		switch (arg0.getKeyChar()) {
		case ' ': {
			view.getCamera().resetView();
			break;
		}
		case 'c': {

			if ((this.hides.contains("compoundWords")) && !(this.hides.contains("tagJDM"))) {
				switchHideNode("compoundWords");
				if (!(this.hides.contains("tagJDMSimple"))) {
					switchHideNode("tagJDMSimple");
					switchHideNode("tagJDMSimple");
				}
				switchHideNode("tagJDM");
				switchHideNode("tagJDM");
			} else if ((this.hides.contains("compoundWords")) && !(this.hides.contains("tagJDMSimple"))) {
				switchHideNode("compoundWords");
				if (!(this.hides.contains("tagJDMSimple"))) {
					switchHideNode("tagJDM");
					switchHideNode("tagJDM");
				}
				switchHideNode("tagJDMSimple");
				switchHideNode("tagJDMSimple");
			} else if ((this.hides.contains("compoundWords")) && (this.hides.contains("superRaffsem"))) {
				switchHideNode("superRaffsem");
				switchHideNode("superRaffmorpho");
				switchHideNode("compoundWords");
			} else {
				switchHideNode("compoundWords");
			}

			break;
		}
		case 't': {
			switchHideNode("tagTT");
			break;
		}
		case 'r': {
			if ((this.hides.contains("compoundWords")) && (this.hides.contains("superRaffsem"))) {
				switchHideNode("superRaffmorpho");
				switchHideNode("superRaffsem");
				switchHideNode("compoundWords");
			} else if (!(this.hides.contains("compoundWords")) && (this.hides.contains("tagJDMSimple"))
					&& (this.hides.contains("superRaffsem"))) {
				switchHideNode("superRaffmorpho");
				switchHideNode("superRaffsem");
				switchHideNode("compoundWords");
				switchHideNode("compoundWords");
			} else if (!(this.hides.contains("compoundWords")) && !(this.hides.contains("tagJDMSimple"))
					&& (this.hides.contains("superRaffsem"))) {
				switchHideNode("superRaffmorpho");
				switchHideNode("superRaffsem");
				switchHideNode("compoundWords");
				switchHideNode("compoundWords");
				if (!(this.hides.contains("tagJDM"))) {
					switchHideNode("tagJDM");
					switchHideNode("tagJDM");
				}
				switchHideNode("tagJDMSimple");
				switchHideNode("tagJDMSimple");
			} else {
				switchHideNode("superRaffmorpho");
				switchHideNode("superRaffsem");
			}

			break;
		}
		case 'j': {
			if (arg0.isAltDown()) {

				if (this.hides.contains("compoundWords") && this.hides.contains("tagJDM")) {
					switchHideNode("superRaffsem");
					switchHideNode("superRaffmorpho");
					switchHideNode("compoundWords");
				}

				// if two hide, display two
				if (!(this.hides.contains("tagJDMSimple")) && !(this.hides.contains("tagJDM"))) {
					switchHideNode("tagJDMSimple");
				} else if ((this.hides.contains("tagJDMSimple")) && (this.hides.contains("tagJDM"))) {
					switchHideNode("tagJDMSimple");
				}

				switchHideNode("tagJDM");
			} else {

				// if tagJDMsimple is display, hide jdmSimple tag
				if (!(this.hides.contains("tagJDM")) && !(this.hides.contains("tagJDMSimple"))) {
					switchHideNode("tagJDM");

				} else {
					// active compound word
					if (this.hides.contains("compoundWords") && this.hides.contains("superRaffsem")
							&& this.hides.contains("tagJDMSimple")) {
						switchHideNode("superRaffsem");
						switchHideNode("superRaffmorpho");
						switchHideNode("compoundWords");

					} else if (this.hides.contains("compoundWords") && !this.hides.contains("superRaffsem")
							&& this.hides.contains("tagJDMSimple")) {
						switchHideNode("compoundWords");
					}
				}

				switchHideNode("tagJDMSimple");
			}
			break;
		}
		default: {
			break;
		}
		}
		view.updateUI();
	}
}
