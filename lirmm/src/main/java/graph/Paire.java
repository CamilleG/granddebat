package graph;

public class Paire<F, S> {

	////////////////
	// Attributes //
	////////////////
	private F first;
	private S second;

	/////////////////////
	// getter / setter //
	/////////////////////
	public F getFirst() {
		return first;
	}

	public void setFirst(F first) {
		this.first = first;
	}

	public S getSecond() {
		return second;
	}

	public void setSecond(S second) {
		this.second = second;
	}

	//////////////////
	// Constructors //
	//////////////////
	public Paire(F first, S second) {
		super();
		this.first = first;
		this.second = second;
	}

	/////////////
	// Methods //
	/////////////
	public Boolean contains(Object obj) {
		return this.first.equals(obj) || this.second.equals(obj);
	}
}
