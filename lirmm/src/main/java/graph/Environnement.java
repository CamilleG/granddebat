package graph;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;

import requeterrezo.RequeterRezo;

public class Environnement {

	////////////////
	// Attributes //
	////////////////
	private ArrayList<HashMap<String, String>> values;
	private RequeterRezo rezo;

	//////////////////
	// Constructors //
	//////////////////
	public Environnement(RequeterRezo rezo) {
		this.values = new ArrayList<>();
		this.rezo = rezo;
	}

	/////////////
	// Methods //
	/////////////

	/**
	 * check if environment is empty
	 *
	 * @return true is environment is empty, else false;
	 */
	public Boolean isEmpty() {
		return this.values.isEmpty();
	}

	@Override
	public String toString() {
		return this.values.toString();
	}

	/**
	 * apply the constraint of graph to the environment
	 *
	 * @param constraint
	 * @param graph
	 */
	public void check(Triplet constraint, Graph graph) {
		if (constraint.getType().equals("TripletEquals")) {
			this.check((TripletEquals) constraint, graph);
		} else if (constraint.getType().equals("TripletLink")) {
			this.check((TripletLink) constraint, graph);
		} else {
			System.err.println(constraint.getType() + " is not supported in hypothesis !");
		}
	}

	/**
	 * apply action tripletLink with this environment to the graph
	 *
	 * @param action
	 * @param graph
	 */
	public Boolean apply(Triplet action, Graph graph) {
		if (action.getType().equals("TripletMakeNode")) {
			return this.apply((TripletMakeNode) action, graph);
		} else if (action.getType().equals("TripletLink")) {
			return this.apply((TripletLink) action, graph);
		} else if (action.getType().equals("TripletCopyNode")) {
			return this.apply((TripletCopyNode) action, graph);
		} else {
			System.err.println(action.getType() + " is not supported in action !");
		}

		return false;
	}

	/**
	 * apply the constraintLink of graph to the environment
	 *
	 * @param constraint
	 * @param graph
	 */
	private void check(TripletLink constraint, Graph graph) {
		String link = constraint.getRelation();

		boolean neg = false;
		boolean notExist = false;
		if (link.startsWith("*")) {
			neg = true;
			link = link.substring(1);
		} else if (link.startsWith("!")) {
			notExist = true;
			link = link.substring(1);
		}

		boolean inKB = false;
		if (link.startsWith("k")) {
			inKB = true;
			constraint.setRelation("r" + link.substring(1));
		}

		if (constraint.destinationIsVariable() && constraint.sourceIsVariable()) {
			if (inKB) {
				this.checkConsLinkJDM(constraint, graph, neg, notExist);
			} else {
				this.checkConsLink(constraint, graph);
			}
		} else if (constraint.destinationIsVariable()) {
			if (inKB) {
				this.checkConsAttrRightJDM(constraint, graph, neg, notExist);
			} else {
				this.checkConsAttrRight(constraint, graph);
			}
		} else if (constraint.sourceIsVariable()) {
			if (inKB) {
				this.checkConsAttrLeftJDM(constraint, graph, neg, notExist);
			} else {
				this.checkConsAttrLeft(constraint, graph);
			}
		} else {
			System.err.println(constraint + " has no variable !");
		}
	}

	/**
	 * apply the constraintLink of graph to the environment
	 *
	 * @param constraint
	 * @param graph
	 */
	private void check(TripletEquals constraint, Graph graph) {
		String variable = constraint.getVariable();
		String value = constraint.getvalue();

		// if variable is not in environment
		if (!this.hasVar(variable)) {
			Collection<Node> match_node = graph.getNodeSet();
			for (Node node : match_node) {
				if (this.equalNode(node, value)) {
					// build tuple
					HashMap<String, String> tuple = new HashMap<>();
					tuple.put(variable, node.getId());

					this.values.add(tuple);
				}
			}
		} else // if variable is already in environment
		{
			for (HashMap<String, String> tuple : this.getTuples(variable)) {
				Node node = graph.getNode(tuple.get(variable));

				if (!this.equalNode(node, value)) {
					this.values.remove(tuple);
				}
			}
		}
	}

	/**
	 * apply constraint with one variable at left
	 *
	 * @param constraint
	 * @param graph
	 */
	private void checkConsAttrLeft(TripletLink constraint, Graph graph) {
		String variable = constraint.getSource();
		String value = constraint.getDestination();
		String relation = constraint.getRelation();

		// if variable is not in environment
		if (!this.hasVar(variable)) {
			Collection<Edge> match_edges = graph.getEdgeSet();

			for (Edge edge : match_edges) {
				if (this.equalEdge(edge, relation) && this.equalNode(edge.getNode1(), value)) {
					// build tuple
					HashMap<String, String> tuple = new HashMap<>();
					tuple.put(variable, edge.getNode0().getId());

					this.values.add(tuple);
				}
			}

			// ajouté la fabrication des noeuds
		} else // if variable is already in environment
		{
			for (HashMap<String, String> tuple : this.getTuples(variable)) {
				Boolean remove = true;
				Node node = graph.getNode(tuple.get(variable));

				for (Edge edge : node.getLeavingEdgeSet()) {
					if (this.equalEdge(edge, relation) && this.equalNode(edge.getNode1(), value)) {
						remove = false;
					}
				}

				if (remove) {
					// update simple values
					this.values.remove(tuple);
				}
			}
		}
	}

	/**
	 * apply constraint with one variable at left and JDM
	 *
	 * @param constraint
	 * @param graph
	 */
	private void checkConsAttrLeftJDM(TripletLink constraint, Graph graph, Boolean neg, Boolean notExist) {
		String variable = constraint.getSource();
		String value = constraint.getDestination();
		String relation = constraint.getRelation();

		// if variable is not in environment
		if (!this.hasVar(variable)) {
			System.err.println("Warning : the varaible " + variable + " must be exist before constraint " + constraint);
		} else // if variable is already in environment
		{
			for (HashMap<String, String> tuple : this.getTuples(variable)) {
				Node node = graph.getNode(tuple.get(variable));

				int weight = this.rezo.verifierExistenceRelation(node.getAttribute("value"), relation, value);

				if (neg && weight > 0) {
					this.values.remove(tuple);
				} else if (notExist && weight != 0) {
					this.values.remove(tuple);
				} else if (!notExist && !neg && weight <= 0) {
					this.values.remove(tuple);
				}
			}
		}
	}

	/**
	 * apply constraint with one variable at right
	 *
	 * @param constraint
	 * @param graph
	 */
	private void checkConsAttrRight(TripletLink constraint, Graph graph) {
		String variable = constraint.getDestination();
		String value = constraint.getSource();
		String relation = constraint.getRelation();

		// if variable is not in environment
		if (!this.hasVar(variable)) {
			Collection<Edge> match_edges = graph.getEdgeSet();
			for (Edge edge : match_edges) {
				if (this.equalEdge(edge, relation) && this.equalNode(edge.getNode0(), value)) {
					// build tuple
					HashMap<String, String> tuple = new HashMap<>();
					tuple.put(variable, edge.getNode1().getId());

					this.values.add(tuple);
				}
			}
		} else // if variable is already in environment
		{
			for (HashMap<String, String> tuple : this.getTuples(variable)) {
				Boolean remove = true;
				Node node = graph.getNode(tuple.get(variable));

				for (Edge edge : node.getEnteringEdgeSet()) {
					if (this.equalEdge(edge, relation) && this.equalNode(edge.getNode0(), value)) {
						remove = false;
					}
				}

				if (remove) {
					// update simple values
					this.values.remove(tuple);
				}
			}
		}
	}

	/**
	 * apply constraint with one variable at left and JDM
	 *
	 * @param constraint
	 * @param graph
	 */
	private void checkConsAttrRightJDM(TripletLink constraint, Graph graph, Boolean neg, Boolean notExist) {
		String variable = constraint.getDestination();
		String value = constraint.getSource();
		String relation = constraint.getRelation();

		// if variable is not in environment
		if (!this.hasVar(variable)) {
			System.err.println("Warning : the varaible " + variable + " must be exist before constraint " + constraint);
		} else // if variable is already in environment
		{
			for (HashMap<String, String> tuple : this.getTuples(variable)) {
				Node node = graph.getNode(tuple.get(variable));

				int weight = this.rezo.verifierExistenceRelation(value, relation, node.getAttribute("value"));

				if (neg && weight > 0) {
					this.values.remove(tuple);
				} else if (notExist && weight != 0) {
					this.values.remove(tuple);
				} else if (!notExist && !neg && weight <= 0) {
					this.values.remove(tuple);
				}
			}
		}
	}

	/**
	 * apply constraint with two variables
	 *
	 * @param constraint
	 * @param graph
	 */
	private void checkConsLink(TripletLink constraint, Graph graph) {
		String variable0 = constraint.getSource();
		String variable1 = constraint.getDestination();
		String relation = constraint.getRelation();

		// if both variable is not in environment
		if (!this.hasVar(variable0) && !this.hasVar(variable1)) {
			Collection<Edge> edges = graph.getEdgeSet();
			for (Edge edge : edges) {
				if (this.equalEdge(edge, relation)) {
					// build tuple
					HashMap<String, String> tuple = new HashMap<>();
					tuple.put(variable0, edge.getNode0().getId());
					tuple.put(variable1, edge.getNode1().getId());

					this.values.add(tuple);
				}
			}
		} else if (this.hasVar(variable0) && this.hasVar(variable1)) {
			// remove incompatible value from source
			for (HashMap<String, String> tuple : this.getTuples(variable0, variable1)) {
				Node node0 = graph.getNode(tuple.get(variable0));
				Node node1 = graph.getNode(tuple.get(variable1));

				Edge edge = node0.getEdgeToward(node1);
				if (edge != null && this.equalEdge(edge, relation)) {
					this.values.remove(tuple);
				}
			}
		} else if (this.hasVar(variable0)) {
			// remove not compatible values
			for (HashMap<String, String> tuple : this.getTuples(variable0)) {
				Boolean remove = true;

				Node node = graph.getNode(tuple.get(variable0));

				for (Edge edge : node.getLeavingEdgeSet()) {
					if (this.equalEdge(edge, relation)) {
						tuple.put(variable1, edge.getNode1().getId());
						remove = false;
					}
				}

				if (remove) {
					this.values.remove(tuple);
				}
			}
		} else // if (this.hasVar(variable1))
		{
			// remove not compatible values
			for (HashMap<String, String> tuple : this.getTuples(variable1)) {
				Boolean remove = true;

				Node node = graph.getNode(tuple.get(variable1));

				for (Edge edge : node.getEnteringEdgeSet()) {
					if (this.equalEdge(edge, relation)) {
						tuple.put(variable0, edge.getNode0().getId());
						remove = false;
					}
				}

				if (remove) {
					this.values.remove(tuple);
				}
			}
		}
	}

	/**
	 * apply constraint with two variables in JDM
	 *
	 * @param constraint
	 * @param graph
	 */
	private void checkConsLinkJDM(TripletLink constraint, Graph graph, Boolean neg, Boolean notExist) {
		String variable0 = constraint.getSource();
		String variable1 = constraint.getDestination();
		String relation = constraint.getRelation();

		// if both variable is in environment
		if (this.hasVar(variable0) && this.hasVar(variable1)) {
			// remove incompatible value from source
			for (HashMap<String, String> tuple : this.getTuples(variable0, variable1)) {
				Node node0 = graph.getNode(tuple.get(variable0));
				Node node1 = graph.getNode(tuple.get(variable1));

				int weight = this.rezo.verifierExistenceRelation(node0.getAttribute("value"), relation,
						node1.getAttribute("value"));

				if (neg && weight > 0) {
					this.values.remove(tuple);
				} else if (notExist && weight != 0) {
					this.values.remove(tuple);
				} else if (!notExist && !neg && weight <= 0) {
					this.values.remove(tuple);
				}
			}
		} else {
			System.err.println("Warning : varaibles " + variable0 + " and " + variable1
					+ " must be exist before constraint " + constraint);
		}
	}

	/**
	 * 
	 * apply action TripletCopyNode with this environment to the graph
	 *
	 * @param action
	 * @param graph
	 */
	private Boolean apply(TripletCopyNode action, Graph graph) {
		Boolean change = false;
		String name = action.getSource();
		String nodeToCopy = action.getNodeToCopy();
		String label = action.getLabel();

		for (HashMap<String, String> value : this.values) {
			if (value.containsKey(nodeToCopy)) {
				String val = value.get(nodeToCopy);
				if (this.copyNode(val, label, graph)) {
					change = true;
				}
				value.put(name, "copy_" + val);
			}
		}
//		ArrayList<String> valuesVar = this.getValues(nodeToCopy);
//		
//		for (String val : valuesVar) {
//			if (this.copyNode(val, label, graph)) {
//				change = true;
//			}
//			for (HashMap<String, String> value : this.values) {
//				// add new node in value
//				value.put(name, "copy_" + val);
//			}
//		}

		return change;
	}

	/**
	 * apply action tripletLink with this environment to the graph
	 *
	 * @param action
	 * @param graph
	 */
	private Boolean apply(TripletLink action, Graph graph) {
		Boolean change = false;
		String source = action.getSource();
		String destination = action.getDestination();
		String relation = action.getRelation();

		// search two variables
		if (action.sourceIsVariable() && action.destinationIsVariable()) {
			if (!this.hasVar(source)) {
				System.err.println("Warning : variable " + source + " is unknow for " + action + " !");
			}

			if (!this.hasVar(destination)) {
				System.err.println("Warning : variable " + destination + " is unknow for " + action + " !");
			}

			for (HashMap<String, String> value : this.values) {
				if (value.containsKey(source) && value.containsKey(destination)) {
					if (this.createLink(value.get(source), value.get(destination), relation, graph)) {
						change = true;
					}
				}
			}
		} else if (action.sourceIsVariable()) {
			if (!this.hasVar(source)) {
				System.err.println("Warning : variable " + source + " is unknow for " + action + " !");
			}

			for (HashMap<String, String> value : this.values) {
				if (value.containsKey(source)) {
					if (this.createLink(value.get(source), destination, relation, graph)) {
						change = true;
					}
				}
			}
		} else if (action.destinationIsVariable()) {
			if (!this.hasVar(destination)) {
				System.err.println("Warning : variable " + destination + " is unknow for " + action + " !");
			}

			for (HashMap<String, String> value : this.values) {
				if (value.containsKey(destination)) {
					if (this.createLink(source, value.get(destination), relation, graph)) {
						change = true;
					}
				}
			}
		} else {
			if (this.createLink(source, destination, relation, graph)) {
				change = true;
			}
		}
		return change;
	}

	/**
	 * apply action makeNode with this environment to the graph
	 *
	 * @param action
	 * @param graph
	 */
	private Boolean apply(TripletMakeNode action, Graph graph) {
		Boolean change = false;

		String name = action.getNodeName();
		ArrayList<String> concatNode = action.getConcatNode();

		for (HashMap<String, String> possibility : this.values) {
			Boolean allPossibility = true;
			String idNode = "";
			String nodeName = "";
			String labelsNodes = "";
			for (String varNode : concatNode) {
				if (possibility.containsKey(varNode)) {
					if (!nodeName.isEmpty()) {
						nodeName += "_";
					}
					idNode = possibility.get(varNode);
					nodeName += idNode;

					Node nodeSource = graph.getNode(idNode);
					if (nodeSource != null) {
						if (!labelsNodes.isEmpty()) {
							labelsNodes += " ";
						}
						labelsNodes += nodeSource.getAttribute("ui.label");
					}
				} else {
					allPossibility = false;
				}
			}

			if (allPossibility) {
				// create node
				if (this.createNode(nodeName, labelsNodes, graph)) {
					change = true;
					System.out.println(
							"Application de la règle : " + action + " avec les paramètres suivants : " + labelsNodes);
				}
				possibility.put(name, nodeName);
			}
		}

		return change;

	}

	/**
	 * add link relation between source and destination in graph
	 *
	 * @param source_value
	 * @param destination_value
	 * @param relation
	 * @param graph
	 */
	private Boolean createLink(String source_value, String destination_value, String relation, Graph graph) {
		Boolean change = false;
		// if node dosen't exist, then create it
		if (graph.getNode(source_value) == null) {
			if (this.createNode(source_value, graph)) {
				change = true;
			}
		}

		if (graph.getNode(destination_value) == null) {
			if (this.createNode(destination_value, graph)) {
				change = true;
			}
		}

		String id_edge = relation + "_" + source_value + "_" + destination_value;
		if (graph.getEdge(id_edge) == null) {
			Edge edge = graph.addEdge(id_edge, source_value, destination_value, true);
			edge.addAttribute("ui.class", relation);
			edge.addAttribute("value", relation);
			change = true;
		}
		return change;
	}

	/**
	 * Create a new node with the value in the graph
	 * 
	 * @param value
	 * @param graph
	 * @return
	 */
	private boolean createNode(String value, Graph graph) {
		if (graph.getNode(value) == null) {
			Node node = graph.addNode(value);
			node.addAttribute("ui.label", value);
			node.addAttribute("value", value);
			return true;
		}

		return false;
	}

	/**
	 * Create a new node with the value in the graph
	 * 
	 * @param value
	 * @param label
	 * @param graph
	 * @return
	 */
	private boolean createNode(String value, String label, Graph graph) {
		if (graph.getNode(value) == null) {
			Node node = graph.addNode(value);
			node.addAttribute("ui.label", label);
			node.addAttribute("value", value);
			return true;
		}

		return false;
	}

	/**
	 * Copy attributes of the node in graph with idNode like ID Create a new node
	 * with a new label
	 * 
	 * @param idNode
	 * @param label
	 * @param graph
	 * @return
	 */
	private boolean copyNode(String idNode, String label, Graph graph) {
		if ((graph.getNode(idNode) != null) && (graph.getNode("copy_" + idNode) == null)) {
			Node n = graph.getNode(idNode);
			Node nCopy = graph.addNode("copy_" + idNode);
			if (nCopy != null) {
				Iterator<String> t = n.getAttributeKeyIterator();
				while (t.hasNext()) {
					String key = t.next();
					if (!key.equals("ui.label")) {
						Object o = n.getAttribute(key);
						nCopy.addAttribute(key, o);
					}
				}
				nCopy.addAttribute("ui.label", label);
				// Copier les ponts : se fait directement dans les règles
				// pareil pour le postagging

				return true;
			}
		}
		return false;
	}

	/**
	 * check if a node have a given value
	 *
	 * @param node
	 * @param value
	 * @return true if node is equal to value
	 */
	private Boolean equalNode(Node node, String value) {
		return node.getAttribute("value").equals(value);
	}

	/**
	 * check if a edge have a given value
	 *
	 * @param edge
	 * @param relation
	 * @return true if edge is equal to value
	 */
	private Boolean equalEdge(Edge edge, String relation) {
		return edge.getAttribute("value").equals(relation);
	}

	/**
	 * check if variable var is in environment
	 *
	 * @param var
	 * @return true if var is a variable in environment, else false.
	 */
	private Boolean hasVar(String var) {
		for (HashMap<String, String> value : this.values) {
			if (value.containsKey(var)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * get all tuple that contains variable var
	 *
	 * @param var
	 * @return list of tuple that contains variable var
	 */
	private ArrayList<HashMap<String, String>> getTuples(String var) {
		ArrayList<HashMap<String, String>> result = new ArrayList<>();

		for (HashMap<String, String> tuple : this.values) {
			if (tuple.containsKey(var)) {
				result.add(tuple);
			}
		}

		return result;
	}

	/**
	 * get all tuple that contains variable var1 and var2
	 *
	 * @param var0
	 * @param var1
	 * @return list of tuple that contains variable var1 and var2
	 */
	private ArrayList<HashMap<String, String>> getTuples(String var0, String var1) {
		ArrayList<HashMap<String, String>> result = new ArrayList<>();

		for (HashMap<String, String> tuple : this.values) {
			if (tuple.containsKey(var0) && tuple.containsKey(var1)) {
				result.add(tuple);
			}
		}

		return result;
	}
}
