package graph;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DecimalFormat;

import org.annolab.tt4j.TreeTaggerWrapper;
import org.graphstream.graph.Edge;
import org.graphstream.graph.ElementNotFoundException;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.MultiNode;
import org.graphstream.stream.GraphParseException;

import requeterrezo.ConfigurationException;
import requeterrezo.ConfigurationSQL;
import requeterrezo.RequeterRezo;
import requeterrezo.RequeterRezoSQL;

public class TestGraph {

	public static int NUMBER_OF_THREAD_DEFAULT = 5;
	public static String OUTPUT_DIR_DEFAULT = "TreeTaggerPreprocessOutput";
	public static String INPUT_DIR_DEFAULT = "../data";

	@SuppressWarnings("unused")
	private static Graph loadGraph(String repertory, String questionID, String graphName) {
		Graph graph = new GraphCss(graphName);
		try {
			graph.read("../outputMulti/" + repertory + "/" + questionID + "/" + graphName);
		} catch (ElementNotFoundException | IOException | GraphParseException e) {
			e.printStackTrace();
		}
		return graph;
	}

	@SuppressWarnings("unused")
	private static Graph buildGraphExample() {
		Graph graph = new GraphCss("Graph_test");

		MultiNode source = graph.addNode("1");
		source.addAttribute("ui.class", "racine");
		source.addAttribute("ui.label", "chat");
		source.addAttribute("value", "chat");

		MultiNode target = graph.addNode("2");
		target.addAttribute("ui.label", "queue");
		target.addAttribute("value", "queue");

		Edge e = graph.addEdge("1->2", source, target, true);
		e.addAttribute("ui.label", "r_has_part");
		e.addAttribute("ui.class", "r_has_part");
		e.addAttribute("value", "r_has_part");

		return graph;
	}

	public static void testPosTaggingGlobal(String[] arg0) throws IOException {
		//////////////// TEST POSTAGGING GLOBAL ////////////////
		TreeTaggerWrapper<String> tt = new TreeTaggerWrapper<String>();
		System.setProperty("treetagger.home", "../ressources/tree-tagger");
		tt.setModel("../ressources/lib/french.par:utf-8");
		ToolsText toolsTexte = new ToolsText(getRequeterRezo());
		DecimalFormat formater = new DecimalFormat();

		String inputDirPath = INPUT_DIR_DEFAULT;
		String outputDirPath = OUTPUT_DIR_DEFAULT;
		String outputBase;

		if (arg0.length > 0) {
			inputDirPath = arg0[0];
		}
		if (arg0.length > 1) {
			outputDirPath = arg0[1];
		}
		File inputDir = new File(inputDirPath);
		File outputDir = new File(outputDirPath);
		if (!outputDir.exists()) {
			outputDir.mkdir();
		}
		if (inputDir.exists() && inputDir.isDirectory()) {
			for (File file : inputDir.listFiles()) {
				outputBase = outputDirPath + File.separator + file.getName().substring(0, file.getName().length() - 5);
				outputDir = new File(outputBase);
				if (!outputDir.exists()) {
					outputDir.mkdir();
				}
				System.out.println("Analyzing: \"" + file.getName() + "\"...");

				long startTime = System.nanoTime();
				// File file = new File(App.DATAPATH +
				// "CUT2_1LA_FISCALITE_ET_LES_DEPENSES_PUBLIQUES" + ".json");
				Theme theme = new Theme(file, toolsTexte);
				theme.testPosTaggingResponse(tt, outputBase + File.separator, false);

				long endTime = System.nanoTime();
				long timeElapsed = endTime - startTime;

				GraphCss graph = theme.getGraphWithTraitements(5);

				graph.display();

				// System.out.println("Execution time in nanoseconds : " + timeElapsed);
				System.out.println("Execution time in milliseconds: " + formater.format(timeElapsed / 1_000_000));
				System.out.println("Execution time in seconds: " + formater.format(timeElapsed / 1_000_000_000));
			}
		}
		toolsTexte.close();
		tt.destroy();
	}

	@SuppressWarnings("unused")
	private static void testLittleFile() throws IOException {
//		 String outputDir = "TreeTaggerPreprocessOutput";
		String[] inputDir = { "../data/littleFile" };
		testPosTaggingGlobal(inputDir);

	}

	@SuppressWarnings("unused")
	private static void testOneSentence() {
		String s = "La petite girafe de New York boit du lait de chèvre";

		String graphName = "Graph_" + 1 + "_lala";
		ToolsText toolsText = new ToolsText(getRequeterRezo());
		Question question = new Question("lala", "pourquoi ??", false, toolsText);
		question.addAnswer(s);

		GraphCss graph = question.getAnswers().get(0).getGraphWithTraitements(toolsText);
		graph.addAttribute("name", graphName);

		graph.display();

		toolsText.close();
	}

	@SuppressWarnings("unused")
	private static void pythonExecutor(String pythonScriptPath) throws IOException {
		String[] cmd = new String[2];
		cmd[0] = "python"; // check version of installed python: python -V
		cmd[1] = pythonScriptPath;

		// create runtime to execute external command
		Runtime rt = Runtime.getRuntime();
		Process pr = rt.exec(cmd);

		// retrieve output from python script
		BufferedReader bfr = new BufferedReader(new InputStreamReader(pr.getInputStream()));
		String line = "";
		while ((line = bfr.readLine()) != null) {
			// display each output line form python script
			System.out.println(line);
		}
	}

	@SuppressWarnings("unused")
	private static Graph buildExampleGraph() {
		// build graph
		GraphCss graph = new GraphCss("test");
		Node text1 = graph.addNode("text1");
		text1.addAttribute("ui.label", "Le");
		text1.addAttribute("value", "Le");
		Node text2 = graph.addNode("text2");
		text2.addAttribute("ui.label", "chat");
		text2.addAttribute("value", "chat");
		Node text3 = graph.addNode("text3");
		text3.addAttribute("ui.label", "mange");
		text2.addAttribute("value", "mange");
		Node nom = graph.addNode("NOM");
		nom.addAttribute("ui.label", "nom");
		nom.addAttribute("value", "Nom:");

		Edge edge1 = graph.addEdge("text1->text2", text1, text2, true);
		edge1.addAttribute("ui.class", "r_succ");
		edge1.addAttribute("value", "r_succ");
		Edge edge2 = graph.addEdge("text2->text3", text2, text3, true);
		edge2.addAttribute("ui.class", "r_succ");
		edge2.addAttribute("value", "r_succ");
		Edge edge3 = graph.addEdge("text2->nom", text2, nom, true);
		edge3.addAttribute("ui.class", "r_pos");
		edge3.addAttribute("value", "r_pos");
		Edge edge4 = graph.addEdge("text3->nom", text3, nom, true);
		edge4.addAttribute("ui.class", "r_pos");
		edge4.addAttribute("value", "r_pos");

		return graph;
	}

	@SuppressWarnings("unused")
	private static Graph buildGraphData() {
		TreeTaggerWrapper<String> tt = new TreeTaggerWrapper<String>();
		System.setProperty("treetagger.home", "../ressources/tree-tagger");
		try {
			tt.setModel("../ressources/lib/french.par:utf-8");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String file = "../data/littleFile/3CUT2_1LA_FISCALITE_ET_LES_DEPENSES_PUBLIQUES.json";

		Theme theme = new Theme(new File(file), new ToolsText(getRequeterRezo()));
		theme.testPosTaggingResponse(tt, "." + File.separator, false);

		GraphCss graph = theme.getGraphWithTraitements(1);

		return graph;
	}

	@SuppressWarnings("unused")
	private static void testConstraintAttr() {
		Graph graph = buildExampleGraph();
		graph.display();

		// tests constraints
//		Triplet consAttr = new Triplet("$y r_succ mange")
		// $x r_pos Nom: -> $x r_pos GN: & $x r_gov $x
		TripletLink hypothesis1 = new TripletLink("$x r_pos Nom:");
//		TripletLink hypothesis2 = new TripletLink("$i r_succ $e");
//		Triplet consLink = new Triplet("$x r_succ $y");
//		Triplet consLink1 = new Triplet("$x r_pos $z");
//		Triplet consLink2 = new Triplet("$y r_pos $z");

		TripletLink action1 = new TripletLink("$x r_pos GN:");
		TripletLink action2 = new TripletLink("$x r_gov $x");

		Environnement environement = new Environnement(getRequeterRezo());

//		environement.checkConsLink(consLink, graph);
//		System.out.println(environement);

//		environement.checkConsLink(consLink1, graph);
//		System.out.println(environement);

//		environement.checkConsLink(consLink2, graph);
//		System.out.println(environement);

//		environement.checkConsAttr(consAttr, graph);
//		System.out.println(environement);

		environement.check(hypothesis1, graph);
		System.out.println(environement);

//		environement.check(consAttrLeft2, graph);
//		System.out.println(environement);

		environement.apply(action1, graph);
		environement.apply(action2, graph);
	}

	@SuppressWarnings("unused")
	private static void testRule() {
		Graph graph = buildExampleGraph();
		graph.display();

		String string_rule = "$a r_succ $i & $i r_succ $e -> $a r_succ $e";
		Rule rule = new Rule(string_rule, getRequeterRezo());

		rule.apply(graph);
	}

	@SuppressWarnings("unused")
	private static void testRuleParse() {
		Graph graph = buildGraphData();

		File fichier = new File(App.RULESPATH + "grammaire2.txt");

		RuleFile rule_file = new RuleFile(fichier, getRequeterRezo());

		rule_file.apply(graph);
		graph.display();
	}

	public static RequeterRezo getRequeterRezo() {
		ConfigurationSQL config;
		try {
			config = new ConfigurationSQL("lib/RequeterRezo/RequeterRezo.ini");
			RequeterRezo rezo = new RequeterRezoSQL(config);
			return rezo;
		} catch (IOException | ConfigurationException e) {
			System.err.println("Error : Impossible to load the configuration !");
			e.printStackTrace();
			return null;
		}
	}

	public static void checkContraintJdm() {
		Graph graph = buildGraphData();
		Environnement environement = new Environnement(getRequeterRezo());

		TripletLink hypothesis1 = new TripletLink("$x r_succ .");
		environement.check(hypothesis1, graph);
		System.out.println(environement);

		environement.check(new TripletLink("$x *k_syn budget"), graph);
		System.out.println(environement);
//		graph.display();
	}

	public static void testSansKaradock() {
		Graph graph = buildExampleGraph();

		RequeterRezo rezo = null;
		Environnement environement = new Environnement(rezo);

		TripletLink hypothesis1 = new TripletLink("$x r_pos Nom:");
		TripletLink hypothesis2 = new TripletLink("$x r_succ $y");
		TripletLink hypothesis3 = new TripletLink("$y r_pos Nom:");
//		TripletLink hypothesis4 = new TripletLink("$y r_succ $post");
//		TripletLink hypothesis5 = new TripletLink("$pre r_succ $x");

		TripletMakeNode action = new TripletMakeNode("$a makeNode $x.$y");

		environement.check(hypothesis1, graph);
		environement.check(hypothesis2, graph);
		environement.check(hypothesis3, graph);
//		environement.check(hypothesis4, graph);
//		environement.check(hypothesis5, graph);
//		System.out.println(environement);

		environement.apply(action, graph);

		graph.display();
	}

	public static void main(String[] arg0) throws IOException {

//		testConstraintAttr();
//		testLittleFile();
		testRuleParse();
//		testLittleFile();
//		getRequeterRezo();

//		checkContraintJdm();

//		testSansKaradock();
		System.out.println("Terminé !");
	}

}
