package graph;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

import org.annolab.tt4j.TreeTaggerException;
import org.annolab.tt4j.TreeTaggerWrapper;
import org.graphstream.graph.Edge;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.MultiNode;

public class Response implements Serializable {

	private static final long serialVersionUID = 1L;
	////////////////
	// Attributes //
	////////////////
	private ArrayList<String> answer;
	private ArrayList<PosTagging> answerPos;
	private String questionID;
	private String responseID;
//	private ToolsText toolsText;

	//////////////////
	// Constructors //
	//////////////////
	public Response(ArrayList<String> answer, String questionID, String responseID/* , ToolsText toolsText */) {
		this.answer = answer;
		this.questionID = questionID;
		this.responseID = responseID;
//		this.toolsText = toolsText;
		this.answerPos = new ArrayList<PosTagging>();
	}

	public Response(Response r, ToolsText toolsText) {
		new Response(r.getAnswer(), r.getQuestionID(), r.getResponseID()/* , r.getToolsText() */);
	}

	////////////////////
	// getters/Setters //
	////////////////////
	public ArrayList<String> getAnswer() {
		return answer;
	}

	public String getQuestionID() {
		return questionID;
	}

	public String getResponseID() {
		return responseID;
	}

	public String getID() {
		return responseID + "_" + questionID;
	}

//	public ToolsText getToolsText() {
//		return this.toolsText;
//	}

	public ArrayList<PosTagging> getAnswerPos() {
		return answerPos;
	}

	/////////////
	// Serialization Methods //
	/////////////
	protected static Response chargerReponse(String chemin) {

		FileInputStream fichierFluxEntrant;

		ObjectInputStream objetFluxEntrant;

		Response resp = null;

		try {

			fichierFluxEntrant = new FileInputStream(chemin);

			objetFluxEntrant = new ObjectInputStream(fichierFluxEntrant);

			resp = (Response) objetFluxEntrant.readObject();

			objetFluxEntrant.close();

			fichierFluxEntrant.close();

		} catch (EOFException e) {

			return null;

		} catch (Exception e) {

			e.printStackTrace();

		}

		return resp;

	}

	protected void sauvegarderReponse(String chemin) {

		FileOutputStream fichierFluxSortant;

		ObjectOutputStream objetFluxSortant;

		try {

			fichierFluxSortant = new FileOutputStream(chemin);

			objetFluxSortant = new ObjectOutputStream(fichierFluxSortant);

			objetFluxSortant.writeObject(this);

			objetFluxSortant.close();

			fichierFluxSortant.close();

		} catch (IOException e) {

			e.printStackTrace();

		}

	}

	/////////////
	// Methods //
	/////////////
	@Override
	public String toString() {
		String result = "";

		result += "\t";
		result += this.answerPos;
		result += "\n";

		return result;
	}

	/**
	 * Build a graph from tokens answer
	 *
	 * @return graph of answer
	 */
	public GraphCss getGraph() {
		GraphCss graph = new GraphCss("Graph_" + this.getID());
		String graphNumber = this.getResponseID();

		File f = new File(ToolsText.LOGPATH + "Graph_" + this.getID() + ".txt");
		try {
			f.createNewFile();
			FileWriter fw = new FileWriter(f);
			fw.write(ToolsText.staticDeTokenise(this.answer) + "\n \n");
			fw.flush();
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		ArrayList<String> forbiden_charracters = new ArrayList<>();
		forbiden_charracters.add("\\");

		// start node
		MultiNode startNode = graph.addNode("start_" + graphNumber);
		startNode.addAttribute("ui.label", "start");
		startNode.addAttribute("value", "start");
		startNode.addAttribute("ui.class", "nodeEnds");

		ArrayList<String> tokeniseAnswer = this.getAnswer();
		// text nodes
		MultiNode pred = startNode;
		for (int i = 0; i < tokeniseAnswer.size(); i++) {
			// Forbidden characters

			if (!forbiden_charracters.contains(tokeniseAnswer.get(i))) {
				MultiNode node = graph.addNode("Text_" + i + "_" + graphNumber);
				node.addAttribute("ui.label", tokeniseAnswer.get(i));
				node.addAttribute("value", tokeniseAnswer.get(i));

				if (!this.answerPos.isEmpty()) {
					String pos = this.answerPos.get(i).getPos();
					Node tagNode = graph.getNode(pos);
					if (tagNode == null) {
						tagNode = graph.addNode(pos);
						tagNode.addAttribute("ui.label", pos);
						tagNode.addAttribute("value", pos);
						tagNode.addAttribute("ui.class", "tagTT");
						tagNode.addAttribute("source", "treeTagger");
					}

					Edge tagEdge = graph.addEdge("r_pos_" + node.getId() + "_" + tagNode.getId(), node, tagNode, true);
					tagEdge.addAttribute("ui.class", "r_tag");
					tagEdge.addAttribute("value", "r_pos");

					node.addAttribute("lemma", this.getAnswerPos().get(i).getLemma());
				}

				Edge edge = graph.addEdge("r_succ_" + pred.getId() + "_" + graphNumber, pred, node, true);
				edge.addAttribute("ui.class", "r_succ");
				edge.addAttribute("value", "r_succ");

				pred = node;
			}
		}

		// end node
		MultiNode endNode = graph.addNode("end_" + graphNumber);
		endNode.addAttribute("ui.label", "end");
		endNode.addAttribute("value", "end");
		endNode.addAttribute("ui.class", "nodeEnds");

		Edge edge = graph.addEdge("r_succ_" + pred.getId() + "_" + graphNumber, pred, endNode, true);
		edge.addAttribute("ui.class", "r_succ");
		edge.addAttribute("value", "r_succ");

		return graph;
	}

	/**
	 * build and apply pre-processing
	 *
	 * @param toolsText
	 *
	 * @return graph result
	 */
	public GraphCss getGraphWithTraitements(ToolsText toolsText) {
		GraphCss graph = this.getGraph();

		toolsText.spellCheckingHunspell(graph);
//		toolsText.posTaggerTreeTager(graph);
		toolsText.posTaggerJDM(graph);
		try {
			toolsText.verificationPosTagging(graph);
		} catch (IOException e) {
			e.printStackTrace();
		}
		toolsText.applyRealCaseInGraph(graph);
		toolsText.gatherCompoundWords(graph);
		toolsText.posTaggerJdmCompoundWord(graph);
		toolsText.removeStopWord(graph);
		toolsText.findGovernorCompoundWords(graph);
		toolsText.inferenceJdm(graph);
		toolsText.inferenceJdmCompoundWords(graph);
//		toolsText.lementisation(graph);

		return graph;
	}

	/**
	 * build, apply pre-processing and export in file
	 *
	 * @param name of input filename
	 * @return list of filename of exports
	 */
	public String exportWithTraitements(String inputFilename, ToolsText toolsText) {

		GraphCss graph = this.getGraphWithTraitements(toolsText);
		String foldername = App.EXPORTPATH + inputFilename + "/" + this.getQuestionID() + "/";
		String filename = foldername + this.getID() + ".dgs";

		// create folders
		(new File(foldername)).mkdirs();

		try {
			graph.write(filename);
		} catch (IOException e) {
			System.err.println("Error : impossible to write graph in file.");
			e.printStackTrace();
		}
		graph.clear();
		return filename;
	}

	/**
	 * Tag the part of speech of the response with treeTagger
	 */
	public void posTaggerResponse(TreeTaggerWrapper<String> tt) {
		try {

			tt.setHandler((token, pos, lemma) -> {
//				System.out.println("token== "+token+" | pos == "+pos+" | lemma== "+lemma);
				this.answerPos.add(new PosTagging(token, pos, lemma));
			});
			tt.process(this.answer);

		} catch (IOException | TreeTaggerException e) {
			e.printStackTrace();
		}
	}

}
