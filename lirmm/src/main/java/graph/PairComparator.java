package graph;

import java.util.Comparator;

public class PairComparator implements Comparator<Pair> {

	public int compare(Pair arg0, Pair arg1) {
		return Double.compare(arg1.getValue(), arg0.getValue());
	}
}