package graph;

import java.util.ArrayList;
import java.util.List;

public class TransformToGraph implements Runnable {

	private ArrayList<Response> responses;
	private ToolsText toolsTexte;
	private String inputFileName;

	public TransformToGraph(List<Response> responses, ToolsText toolsTexte, String inputFileName) {
		this.inputFileName = inputFileName;
		this.responses = new ArrayList<>(responses);
		this.toolsTexte = toolsTexte;
	}

	/**
	 * build, apply pre-processing and export in file
	 *
	 * @param preprocessings
	 * @return list of filename of exports
	 */
	@Override
	public void run() {
		for (int i = 0; i < this.responses.size(); i++) {

			Response response = this.responses.get(i);

			// condition if seulement a des fin de tests
			if (i == 0) {
				response.getGraphWithTraitements(toolsTexte);
			} else {
				response.exportWithTraitements("(thread)" + this.inputFileName, toolsTexte);
			}

		}
		toolsTexte.close();
	}
}
