package graph;

public class TripletLink extends Triplet {

	////////////////
	// Attributes //
	////////////////
	private String source;
	private String relation;
	private String destination;

	//////////////////
	// Constructors //
	//////////////////

	public TripletLink(String input) {
		String[] s_r_d = input.split(" ");

		if (s_r_d.length != 3) {
			throw new IllegalArgumentException("Error : " + input + " bad format !");
		} else {
			this.source = s_r_d[0];
			this.relation = s_r_d[1];
			this.destination = s_r_d[2];
		}
	}

	public TripletLink(String s, String r, String d) {
		this.source = s;
		this.relation = r;
		this.destination = d;
	}

	/////////////////////
	// getter / setter //
	/////////////////////
	public String getRelation() {
		return relation;
	}

	public void setRelation(String relation) {
		this.relation = relation;
	}

	public String getSource() {
		return source;
	}

	public String getDestination() {
		return destination;
	}

	/////////////
	// Methods //
	/////////////
	public Boolean sourceIsVariable() {
		return this.source.contains("$");
	}

	public Boolean destinationIsVariable() {
		return this.destination.contains("$");
	}

	@Override
	public String toString() {
		return this.source + " --" + this.relation + "-> " + this.destination;
	}

	@Override
	public String getType() {
		return "TripletLink";
	}

}
