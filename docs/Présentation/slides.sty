\NeedsTeXFormat{LaTeX2e}[1994/06/01]
\ProvidesPackage{slides}[2019/04/05 Slides]

% Langue
\usepackage{polyglossia}
\usepackage[autostyle=true]{csquotes}
\setmainlanguage{french}
\usepackage{microtype}
\usepackage[
    output-decimal-marker={,},
    group-separator={\,},
]{siunitx}

% Police
\usepackage{fontspec}
\usepackage{xunicode}

%% Maths
\usepackage{amsmath}
\usepackage{unicode-math}

\newcommand{\var}[1]{\mathrm{#1}}
\newcommand{\func}[1]{\mathrm{#1}}

% Figures
\usepackage{graphicx}
\graphicspath{{fig/}}

\usepackage{tikz}
\usetikzlibrary{fadings}
\usetikzlibrary{calc}
\usetikzlibrary{positioning}
\usetikzlibrary{shapes}
\usetikzlibrary{patterns}

\usepackage{tikzmark}
\usepackage{pgfplots}
\pgfplotsset{compat=1.15}

\usepackage{forest}
\usepackage{booktabs}

%% Taille par défaut du texte dans les figures
\tikzset{font=\footnotesize}

%% Style par défaut de flèche
\tikzset{>=latex}

%% Permet de découvrir un graphisme TiKZ morceau par morceau
\tikzset{
    dimmed/.style={opacity=.25},
    invisible/.style={opacity=0},
    dimmed on/.style={alt={#1{dimmed}{}}},
    visible on/.style={alt={#1{}{invisible}}},
    alt/.code args={<#1>#2#3}{%
        \alt<#1>{\pgfkeysalso{#2}}{\pgfkeysalso{#3}}
    }
}

%% Permet de centrer une figure TikZ autour d’un rectangle
%  ou d’un point donné
\tikzset{
    % autour d’un rectangle délimité par deux points
    center around/.style 2 args={
        execute at end picture={%
            \useasboundingbox
                let \p0 = (current bounding box.south west),
                    \p1 = (current bounding box.north east),
                    \p2 = (#1), \p3 = (#2)
                    in ({min(\x2 + \x3 - \x1, \x0)}, {min(\y2 + \y3 - \y1, \y0)})
                    rectangle ({max(\x3 + \x2 - \x0, \x1)}, {max(\y3 + \y2 - \y0, \y1});
        },
    },
    % autour d’un point
    center at/.style={
        center around={#1}{#1},
    },
}

% Algorithmes
\usepackage{algorithm}
\usepackage[noend]{algpseudocode}

%% Traductions
\renewcommand{\ALG@name}{Algorithme}
\algrenewcommand\algorithmicprocedure{\textbf{procédure}}
\algrenewcommand\algorithmicrequire{\textbf{Pré-requis~:}}
\algrenewcommand\algorithmicensure{\textbf{Résultat~:}}
\algrenewcommand\algorithmicif{\textbf{si}}
\algrenewcommand\algorithmicthen{\textbf{alors}}
\algrenewcommand\algorithmicelse{\textbf{sinon}}
\algrenewcommand\algorithmicfor{\textbf{pour}}
\algrenewcommand\algorithmicforall{\textbf{pour chaque}}
\algrenewcommand\algorithmicwhile{\textbf{tant que}}
\algrenewcommand\algorithmicdo{\textbf{faire}}
\algrenewcommand\algorithmicend{\textbf{fin}}
\algrenewcommand\algorithmicreturn{\textbf{renvoyer}}

%% Instruction « si/alors » en une ligne
\newcommand{\IfThen}[2]{{\algorithmicif} #1 {\algorithmicthen} #2}

%% Numérotation des lignes
\algrenewcommand\alglinenumber[1]{\footnotesize #1\hspace{.25em}}

%% Constantes
\newcommand{\True}{\var{Vrai}}
\newcommand{\False}{\var{Faux}}

%% Algos distribués
\newcommand{\NetMessage}[1]{\ensuremath{\langle#1\rangle}}

% Schéma d’un anneau
\tikzset{
    process/.style={
        circle,
        draw,
        thick,
        inner sep=0pt,
        minimum width=1.7em,
        minimum height=1.7em,
    },
    active process/.style={
        process,
        fill=green!20,
    },
    passive process/.style={
        process,
        pattern=north west lines,
        pattern color=fg
    },
    leader process/.style={
        process,
        fill=yellow!40,
        very thick,
    },
    channel/.style={
        ->,
        draw,
        thick,
    }
}

%% Compte le nombre de valeurs dans une liste TikZ
% Arguments :
%   - liste à dénombrer
%   - macro dans laquelle stocker le résultat
\newcounter{list@getlength}
\newcommand{\getlength}[2]{%
    \setcounter{list@getlength}{0}% 
    \foreach \dummy in {#1} {%
        \stepcounter{list@getlength}%
    }%
    \edef#2{\arabic{list@getlength}}
}

%% Rayon du cercle représentant l’anneau
\newcommand{\ring@radius}{2}
\newcommand{\ringradius}[1]{\renewcommand{\ring@radius}{#1}}

%% Marge entre les nœuds et les flèches des canaux
\newcommand{\ring@margin}{10}
\newcommand{\ringmargin}[1]{\renewcommand{\ring@margin}{#1}}

%% Dessine un anneau
% Arguments :
%   - états des sites (indice dans l’anneau/identité/style du site)
%   - messages dans l’anneau (émetteur/position relative/contenu)
\newcommand{\ring}[2]{
    \getlength{#1}{\countprocesses}
    \def\anglestep{-360 / \countprocesses}
    %
    % Création des nœuds sites et des canaux de communication
    \foreach \id/\value/\style in {#1} {
        \path[channel]
            ({\id * \anglestep - \ring@margin}:\ring@radius)
            arc[
                start angle={\id * \anglestep - \ring@margin},
                delta angle={\anglestep + 2 * \ring@margin},
                x radius=\ring@radius,
                y radius=\ring@radius
            ];
        \node[\style]
            (p\id) at ({\id * \anglestep}:\ring@radius)
            {\value};
    }
    %
    % Création des messages sur les canaux
    \foreach
        \id/\style/\contents
        in {#2}
    {
        \path
            ({\id * \anglestep}:\ring@radius)
            arc[
                start angle={\id * \anglestep},
                delta angle={\anglestep},
                x radius=\ring@radius,
                y radius=\ring@radius
            ]
            node[
                midway, auto,
                align=center
            ] (m\id) {\contents};
    }
}

% Texte barré
\usepackage{ulem}
\normalem
\newcommand\xoutred{\bgroup \markoverwith{\hbox to.35em{\hss\textcolor{red}{/}\hss}}\ULon}

% Thème des slides
\usetheme{Bruno}
