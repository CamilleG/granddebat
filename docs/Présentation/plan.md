# Introduction

## title slide
-   intitulé du projet
-   qui sommes nous
-   encadrant
-   dates et durée

---

# Présentation du stage

## Présentation du laboratoire et équipe TEXTE
  -   qu'es ce que c'est.
  -   il est ou ?
  -   statistique random

  -   sujet de recherche equipe
  -   définition TALN
  -   notre emplacement (bureau de Jimmy)

## Motivation du projet

-   Appelle manifestation d'intérêt de l'Agence national de la recherche.
-   Grand débat national
	-   Crise social gillet jaunes
	-   État mis en place un GDN
	-   2 mois, 15 janvier - 15 mars 2019
	-   dont la platforme en ligne
	-   1 932 884 Contributions en ligne
-   intéressant
	-   sujet actualité
	-   actualité
	-   grand corpus
	-   donnée reel

-   utilise les donné du grand débat

## objectif du projet
-   outil capable d’extraire des connaissances à partir de données textuelles.
-   donnée brutte (sur tout corpus)
-   structuré les donnée efficasement
-   les enrichire
-   intérogation complétion langage de règle 

## plan de presentation

---

# Extraction de données textuelles

## objectif

## fichier de donnée
-   4 fichiers
-   contenue
-   2 format (json, csv)

# iplémntation
-   gson
-   flux

---

# strucutre de donnée

## mission objectif

## idéé texte

## idée graph

# implémentation

---

# Enrichissement des données

## missions et objectifs

## outils utilisé

## liste des traitements réalisé

---

# mise en place langage de règle

## mission

## exempel et description du langage

---

# conclusion

# résumer de ce qui est fait

# photo de résultat

# ce qui nous manque 

# apport et perspective