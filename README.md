# Analyse du Grand Débat National

Dans le cadre de notre première année de master données, connaissances et langage naturel (DECOL) en Cursus Master d’Ingénierie (CMI), nous avons eu l’opportunité de réaliser un stage au sein du Laboratoire d’Informatique, de Robotique et de Microélectronique de Montpellier (LIRMM).

Notre projet a pour objectif général la création d’un outil capable d’extraire des connaissances à partir de données textuelles. En effet, à partir de données brutes sous forme de fichier, l’application « ToGraphify » doit pouvoir extraire les données, les enrichir et les représenter de manière visuelle et efficace. Il doit également permettre à l’utilisateur d’interroger et de compléter les données au travers d’un langage de règles. Pour parvenir à cet objectif, nous avons choisi de le découper en cinq sous objectifs.

**Rapport de stage :** [rapport.pdf](./docs/Rapport/Rapport_de_stage_M1___Grand_D_bat_National.pdf)

N.B: Utilisez le rezoDump afin d'accéder à la base de données de jeuxDeMots. Ou bien, ajoutez votre fichier de configuration.